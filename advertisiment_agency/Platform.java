package homeworks.advertisiment_agency;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

class Platform implements Serializable {
    private String name;
    private int countOfDisplays;
    private Os os;
    private Browser browser;
    private transient List<Path> displays;
    private int countOfClearDisplays;
    private int countOfBlurbDisplays;

    Platform(String name, Browser browser, Os os, int countOfDisplays) {
        this.name = name;
        this.browser = browser;
        this.os = os;
        this.countOfDisplays = countOfDisplays;
        this.displays = new ArrayList<>();
        initPlatform();
    }

    int getCountOfDisplays() {
        return countOfDisplays;
    }

    Os getOs() {
        return os;
    }

    Browser getBrowser() {
        return browser;
    }

    List<Path> getDisplays() {
        return displays;
    }

    String getName() {
        return name;
    }

    int getCountOfClearDisplays() {
        return countOfClearDisplays;
    }

    void clearCountersOfDisplays(int newcountOfDisplays) {
        setCountOfClearDisplays(0);
        countOfBlurbDisplays = 0;
        countOfDisplays = newcountOfDisplays;
    }

    void increaseCountOfBlurbDisplays() {
        countOfBlurbDisplays++;
        setCountOfClearDisplays(getCountOfClearDisplays() - 1);
    }

    void setOs(Os newOs) {
        this.os = newOs;
    }

    void setBrowser(Browser newBrowser) {
        this.browser = newBrowser;
    }

    void setCountOfDisplays(int newCountOfDisplays) {
        this.countOfDisplays = newCountOfDisplays;
    }

    void initPlatform() {
        createNewDirectoryForPlatform();
        createNewDisplay(countOfDisplays);
    }

    private void createNewDirectoryForPlatform() {
        Path pathToNewDirectory = Paths.get(ConstantsSummary.START_DIR + name);
        if (Files.notExists(pathToNewDirectory)) {
            try {
                Files.createDirectory(pathToNewDirectory);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void createNewDisplay(int times) {
        IntStream.rangeClosed(1, times).forEach(i -> {
            try {
                Path newDisplay = Files.createFile(Paths.get(ConstantsSummary.START_DIR + name + ConstantsSummary.NAME_OF_FILE +
                        (displays.size() + 1) + ConstantsSummary.EXTENSION));
                displays.add(newDisplay);
                setCountOfClearDisplays(getCountOfClearDisplays() + 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public String toString() {
        return name +
                ", countOfDisplays=" + countOfDisplays +
                ", countOfClearDisplays=" + getCountOfClearDisplays() +
                ", countOfBlurbDisplays=" + countOfBlurbDisplays;
    }

    public void setCountOfClearDisplays(int countOfClearDisplays) {
        this.countOfClearDisplays = countOfClearDisplays;
    }
}
