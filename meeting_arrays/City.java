package homeworks.meeting;

public enum City {
    DNEPR("Dnepropetrovsk"),
    KYIV("Kyiv"),
    KHARKOV("Kharkov"),
    CHERKASSY("Cherkassy"),
    ODESSA("Odessa"),
    DONETSK("Donetsk"),
    NIKOLAEV("Nikolaev"),
    KHERSON("Kherson"),
    LVOV("Lvov"),
    ROVNO("Rovno"),
    CHERNOVTSI("Chernovtsi");

    private String city;

    City(String city){
        this.city = city;
    }

    public String getCity() {
        return city;
    }
}
