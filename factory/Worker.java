package homeworks.from_andrew.factory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Worker implements Runnable {
    private Good good;

    private Thread thread;

    private Path pathToFile;

    private String reasonForStop;


    public Worker(Good good, String name) {
        this.good = good;

        thread = new Thread(this, "Worker " + name + " good - " + good.getName());

        pathToFile = Paths.get("./Factory/" + thread.getName() + ".txt");

        thread.start();
    }

    @Override
    public void run() {

        try {
            if (!Files.exists(pathToFile)) {
                Files.createFile(pathToFile);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        good.getStages().stream().forEach(stage -> {
            try {
                stage.action();
            } catch (InterruptedException e) {
                thread.interrupt();
            }
        });
    }

    /**
     * This method stops worker(thread) with making a reason record to file.
     */
    public void stopWork() {
        try {
            if (!thread.isInterrupted()) {
                Files.write(pathToFile, reasonForStop.getBytes(), StandardOpenOption.APPEND);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        thread.interrupt();
    }

    /**
     * This method sets a reason for interrupting work
     * */
    public void setReasonForStop(String reasonForStop) {
        this.reasonForStop = "Stop work due to -> ".concat(reasonForStop);
    }

    /**
     * This method prints the executing stage.
     * */
    public void printCurrentStage() {
        try {
            List<String> strings = Files.readAllLines(pathToFile);

            System.out.println(strings.get(strings.size() - 1) + "\t" + thread.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
