package homeworks.from_andrew.factory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public interface Stageable {

    void action() throws InterruptedException;

    /**
     * This method makes a record of executing stage to file
     * */
    default void writeStage(String action) {
        try {
            if(!Thread.currentThread().isInterrupted()) {
                Files.write(Paths.get("./Factory/" +
                                Thread.currentThread().getName() + ".txt"), (action + "\n").getBytes(),
                        StandardOpenOption.APPEND);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
