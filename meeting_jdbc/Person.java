package homeworks.meeting_jdbc;

public class Person {

    private String firstName;
    private String lastName;
    private String gender;
    private int age;
    private String city;
    private int countOfChildren;

    public Person(String firstName, String lastName, String gender, int age, String city, int countOfChildren) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
        this.city = city;
        this.countOfChildren = countOfChildren;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    public int getCountOfChildren() {
        return countOfChildren;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                ", countOfChildren=" + countOfChildren +
                '}';
    }
}
