package homeworks.ticket_office;

import java.util.List;
import java.util.stream.IntStream;

public class Returner implements Runnable {

    private TicketService service;
    private int amountOfTickets;

    Returner(int amountOfTickets) {

        this.service = new TicketService();
        this.amountOfTickets = amountOfTickets;
    }

    @Override
    public void run() {

        try {
            returnTicket();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void returnTicket() throws InterruptedException {

        List<Ticket> ticketOffice = service.getTickets();

        synchronized (ticketOffice) {

            if (ticketOffice.size() + amountOfTickets <= service.MAX_CAPACITY) {

                IntStream.rangeClosed(1, amountOfTickets).forEach(i -> {

                    ticketOffice.add(new Ticket());

                });

                System.out.println(amountOfTickets + " ticket(s) returned by " + Thread.currentThread().getName() +
                        "\n Tickets left " + ticketOffice.size());

                Thread.sleep(500);

                ticketOffice.notifyAll();

            } else {
                System.out.println("The ticket office is over, please wait.");

                ticketOffice.wait();

                returnTicket();
            }
        }
    }
}
