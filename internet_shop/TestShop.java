package homeworks.internet_shop;

import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
/**
 * The TestShop program implements an application that imitates work of an internet shop an provides an abilities to:
 *
 * - add an product which have following characteristics: name, producer, price)
 * - add an concomitant product to registered product.
 * - delete an product with all the concomitant products.
 * - create a consumer bucket for the every user.
 * - show the product with the specified characteristics and all the concomitant products.
 *
 * @author Udaltsov Sergey
 * @version 1.0
 * @since november 2017.
 * */

public class TestShop {

    private static final Scanner SCANNER = new Scanner(new InputStreamReader(System.in));

    public static void main(String[] args) {
        ServiceShop service = new ServiceShop();
        int article;
        int id;
        String name;

        while (true) {
            menu();
            try {
                int key = Integer.parseInt(getUserInfo("Enter a number of action."));
                switch (key) {
                    case 1: {
                        String productName = getUserInfo("Enter a name of product");

                        service.addNewProduct(productName,
                                Integer.parseInt(getUserInfo("Enter an article of product")),
                                getCategory(),
                                Double.parseDouble(getUserInfo("Enter a price")));
                        break;
                    }
                    case 2: {
                        name = getUserInfo("Enter a name of new user");
                        id = Integer.parseInt(getUserInfo("Enter an ID of new user"));
                        service.addNewUser(name, id);
                        break;
                    }
                    case 3: {
                        article = Integer.parseInt(getUserInfo("Enter an article of product which you want to delete"));
                        service.deleteProduct(article);
                        break;
                    }
                    case 4: {
                        System.out.println("We have the following registered users: ");
                        service.showListOfUsers();
                        break;
                    }
                    case 5: {
                        id = Integer.parseInt(getUserInfo("Enter an ID of user"));
                        article = Integer.parseInt(getUserInfo("Enter an article of product you want to add the basket"));
                        service.addProductToUserBasket(id, article);
                        break;
                    }
                    case 6: {
                        id = Integer.parseInt(getUserInfo("Enter an ID of user"));
                        service.showProductsInUserBasket(id);
                        break;
                    }

                    case 7: {
                        System.out.println("In our shop you can find such products:");
                        service.showListOfProducts();
                        break;
                    }

                    case 8: {
                        name = getUserInfo("Enter a name of product to find");
                        service.showProductInfoWithAssociatedProducts(name);
                        break;
                    }

                    case 9: {
                        return;
                    }

                    default: {
                        System.out.println("You've entered a wrong number");
                        break;
                    }

                }
            } catch (NumberFormatException e) {
                System.out.println("You've entered not a number.");


            }
        }
    }


    private static void menu() {
        System.out.println("1. Add new product" + "\n" +
                "2. Register new user" + "\n" +
                "3. Delete product with associated products" + "\n" +
                "4. Show list of users" + "\n" +
                "5. Add a product to user's basket" + "\n" +
                "6. Show products in user's basket" + "\n" +
                "7. Show list of products" + "\n" +
                "8. Find a product by name and show list of associated products" + "\n" +
                "9. Exit");
    }

    private static String getUserInfo(String message) {
        System.out.println(message);
        return SCANNER.nextLine();
    }

    private static Categories getCategory() {
        Categories category = Categories.UNSPECIFIED;
        System.out.println("Choose a category of product\n " +
                "1. Telephone\n" +
                "2. TV set\n" +
                "3. Computer\n" +
                "4. Notebook\n" +
                "5. Audio\n" +
                "6. Washing\n" +
                "7. Refrigerator");
        int key1 = Integer.parseInt(SCANNER.nextLine()) - 1;

        category = Arrays.stream(Categories.values()).filter(c -> c.ordinal() == key1).findFirst().get();

        switch (key1) {
            case 1: {
                category = Categories.TELEPHONE;
                break;
            }
            case 2: {
                category = Categories.TV_SET;
                break;
            }
            case 3: {
                category = Categories.COMPUTER;
                break;
            }
            case 4: {
                category = Categories.NOTEBOOK;
                break;
            }
            case 5: {
                category = Categories.AUDIO;
                break;
            }
            case 6: {
                category = Categories.WASHING;
                break;
            }
            case 7: {
                category = Categories.REFRIGERATOR;
                break;
            }

            default: {
                category = Categories.UNSPECIFIED;
                break;
            }
        }
        return category;
    }

}
