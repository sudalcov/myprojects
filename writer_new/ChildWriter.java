package homeworks.writer_new;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

class ChildWriter extends Writer {

    public void write(String text) throws IOException {

        Path path = Paths.get("./Poem.txt");

        String newText = modifyText(text);

        Files.write(path, newText.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

        ChildReader reader = new ChildReader(path);

        reader.modifyText();
    }

}
