package homeworks.calendar;

import java.time.zone.ZoneRulesException;

class ZoneException extends ZoneRulesException {

    ZoneException(String message) {
        super(message);
    }
}
