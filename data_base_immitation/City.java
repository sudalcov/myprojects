package homeworks.data_base_immitation;

public enum City {
    KHARKIV("Kharkiv"),
    KYIV("Kyiv"),
    DNIPRO("Dnipro"),
    LONDON("London"),
    BERLIN("Berlin"),
    MOSKVA("Moskva"),
    NEW_YORK("New York"),
    SLOVAKSK("Slovaksk"),
    DONETSK("Donetsk");

    private String city;

    City(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return  "city - " + city;
    }
}
