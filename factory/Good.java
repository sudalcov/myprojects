package homeworks.from_andrew.factory;

import java.util.ArrayList;
import java.util.List;

public class Good {
    private String name;

    private List<Stageable> stages;

    public Good(String name) {
        this.name = name;
        stages = new ArrayList<>();
    }

    public void addAction(Stageable stageable) {
        stages.add(stageable);
    }

    public List<Stageable> getStages() {
        return stages;
    }

    public String getName() {
        return name;
    }
}

