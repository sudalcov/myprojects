package homeworks.calendar;

import java.util.Scanner;

/**
 * The TestCalendar program implements an application that provides an abilities to:
 * - get current date and time in several time-zones. Showing a date, it should show all the events appointed on this date.
 * - create an event on the certain date. Show all the events of the required date.
 * - provide an ability to remove an event from the certain date.
 * - define time-zone, current time, day of week in that time-zone by inserting user's city and continent.
 * - show date in a week, month, year from current date.
 * - show by choice time, date, day of week, day of year, number of days left to New Year.
 * - show current date in a custom format entered be user.
 *
 * @author Udaltsov Sergey
 * @version 1.0
 * @since november 2017.
 * */

public class TestCalendar {
    public static final String DATE_FORMAT = "YYYY-MM-DD";
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final int EXIT = 5;

    public static void main(String[] args) {

        CalendarService service = new CalendarService();
        while (true) {
            menu();
            int key = Integer.parseInt(getUserInfoText("Please choose a number of necessary paragraph"));
            switch (key) {

                case 1: {
                    int keyZone = 0;
                    while (keyZone != EXIT) {
                        timeZoneMenu();
                        keyZone = Integer.parseInt(getUserInfoText("Choose time zone: "));
                        switch (keyZone) {

                            case 1: {
                                service.showDateInTimeZoneWithEvents("Europe/Madrid");
                                break;
                            }
                            case 2: {
                                service.showDateInTimeZoneWithEvents("America/New_York");
                                break;
                            }
                            case 3: {
                                service.showDateInTimeZoneWithEvents("Asia/Tokyo");
                                break;

                            }
                            case 4: {
                                service.showDateInTimeZoneWithEvents("Europe/Kiev");
                                break;
                            }
                            case 5: {
                                break;
                            }

                            default: {
                                System.out.println("Sorry, you've entered a wrong number. Try again.\n");
                                break;
                            }

                        }
                    }
                    break;
                }
                case 2: {
                    String date = getUserInfoText("Enter a date of event in format " + DATE_FORMAT);
                    String name = getUserInfoText("Enter a name of event");
                    try {
                        service.createNewEvent(date, name);
                    } catch (Exception e) {
//                        throw new SQLDataException();
                        System.out.println();
                    }
                }
                break;

            case 3: {

                String date = getUserInfoText("Enter a date in format " + DATE_FORMAT + " to show events of this day");
                try {
                    service.showEventsThisDate(date);
                } catch (Exception e) {
                    e.getMessage();
                }
            }
            break;

            case 4: {
                String date = getUserInfoText("Enter a date of removing event in format " + DATE_FORMAT);
                service.deleteEvent(date);
                break;
            }
            case 5: {
                String zoneId = getUserInfoText("Enter a time-zone of your city");
                service.showTimeAndDayOfWeekInSpecifiedTimeZone(zoneId);
                break;
            }

            case 6: {
                service.showDatePlus(1);
                break;
            }
            case 7: {
                service.showDatePlus(2);
                break;
            }
            case 8: {
                service.showDatePlus(3);
                break;
            }
            case 9: {
                service.showDetailsOfToday();
                break;
            }
            case 10: {
                service.formatLettersInfo();
                String format = getUserInfoText("Enter a format for date");
                service.showDate(format);
                break;
            }
            case 11: {
                return;
            }

            default: {
                System.err.println("Sorry, you've entered a wrong number. Try again.\n");
                break;
            }

        }
    }

}

    private static void menu() {
        System.out.println("1. Show date and time in specified time-zone with events" + "\n" +
                "2. Create new event" + "\n" +
                "3. Show list of events" + "\n" +
                "4. Delete event" + "\n" +
                "5. Show time and day of week in specified city/country" + "\n" +
                "6. Show date a week later" + "\n" +
                "7. Show date a month later" + "\n" +
                "8. Show date a year later" + "\n" +
                "9. Show time, date, day of week, day of year, amount days to New Year from today" + "\n" +
                "10. Show date in specified format" + "\n" +
                "11. Exit");

    }

    private static void timeZoneMenu() {
        System.out.println("1. Madrid" + "\n" +
                "2. New York" + "\n" +
                "3. Tokyo" + "\n" +
                "4. Kiev" + "\n" +
                "5. Exit");
    }

    private static String getUserInfoText(String message) {
        System.out.println(message);
        String text = SCANNER.next();
        return text;
    }

}
