package homeworks.data_base_immitation;

public class Address {
    private Country country;
    private City city;
    private String street;
    private int numberOfHouse;
    private int numberOfFlat;

    Address(Country country, City city, String street, int numberOfHouse, int numberOfFlat) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.numberOfHouse = numberOfHouse;
        this.numberOfFlat = numberOfFlat;
    }

    Country getCountry() {
        return country;
    }

    String getStreet() {
        return street;
    }

    int getNumberOfHouse() {
        return numberOfHouse;
    }

    @Override
    public String toString() {
        return ", address: " + country + ", " + city + ", " + street + ", numberOfHouse: " + numberOfHouse +
                ", numberOfFlat: " + numberOfFlat;
    }
}
