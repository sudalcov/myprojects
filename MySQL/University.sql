DROP TABLE university;

CREATE TABLE university(
university_id int AUTO_INCREMENT,
name varchar(30) NOT NULL,
foundation date,
faculties int,
PRIMARY KEY (university_id)
);

INSERT INTO university(name, foundation, faculties)
VALUES
('KHAI',                   '1935-03-25', 6),
('Medicine University',    '1922-06-15', 7),
('KHNURE',                 '1945-06-15', 6),
('KHADI',                  '1960-03-14', 5),
('INJEK',                  '1970-05-03', 6),
('Railway',                '1955-08-12', 8),
('KHPI',                   '1951-08-26', 7),
('State University',       '1957-03-28', 8),
('Pedagogical University', '1982-06-15', 7),
('Mechanical tech',        '2001-05-26', 7),
('Medical tech',           '1992-06-24', 8),
('Phfisical training',     '1985-10-30', 5),
('Culture University',     '1965-01-25', 8),
('Management institute',   '1997-03-25', 4);

SELECT * FROM university;


DROP TABLE faculty;

CREATE TABLE faculty(
faculty_id int AUTO_INCREMENT,
faculty_name varchar(30) NOT NULL,
faculty_foundation date,
university int,
group_count int,
PRIMARY KEY (faculty_id),
FOREIGN KEY (university) REFERENCES university(university_id)
);

INSERT INTO faculty(faculty_name, faculty_foundation, group_count, university)
VALUES
('Surgeonary',            '1935-04-15', 5, 6),
('Stomatology',           '1940-04-15', 4, 5),
('Therapy',               '1935-04-15', 6, 3),
('Traumatology',          '1937-04-15', 5, 4),
('Psychiatry',            '1939-04-15', 6, 1),
('Aircrafting',           '1932-08-20', 5, 10),
('Enginecrafting',        '1935-05-08', 6, 7),
('Systems of planes',     '1940-10-05', 5, 11),
('Rocketcrafting',        '1938-10-24', 5, 1),
('Electronic',            '1937-08-12', 4, 1),
('Management',            '1990-01-01', 6, 9),
('Mechanic',              '1945-01-05', 7, 8),
('Electric',              '1945-02-08', 6, 11),
('Chemic',                '1945-08-06', 3, 6),
('Design',                '1945-06-03', 6, 3),
('Architecture',          '1940-06-26', 6, 2),
('Building constructure', '1941-10-25', 5, 9),
('Computer since',        '1990-08-09', 7, 10),
('Pedagogy',              '1948-07-26', 8, 11),
('Economy',               '1960-10-18', 7, 6),
('Mathematic',            '1930-05-19', 5, 4);

SELECT * FROM faculty;

DROP TABLE groups;

CREATE TABLE groups(
group_id int AUTO_INCREMENT,
group_number int NOT NULL,
group_foundation date ,
students_count int,
leader_l_name varchar(30),
faculty int,
PRIMARY KEY(group_id)
);

INSERT INTO groups(group_number, group_foundation, students_count, faculty)
VALUES
(210, '2015-09-01', 25, 5),
(315, '2013-09-01', 27, 7),
(221, '1999-09-01', 26, 10),
(362, '1985-09-01', 30, 1),
(285, '2010-09-01', 24, 10),
(635, '2002-09-01', 15, 13),
(25, '1998-09-01', 20, 6),
(235, '2012-09-01', 28, 11),
(152, '2000-09-01', 22, 13),
(135, '2008-09-01', 29, 1),
(164, '2001-09-01', 23, 2),
(123, '1995-09-01', 29, 3),
(368, '2012-09-01', 26, 4),
(954, '2011-09-01', 25, 8),
(412, '2005-09-01', 23, 9);

SELECT * FROM groups;

DROP TABLE address;

CREATE TABLE address(
address_id int AUTO_INCREMENT,
city varchar(30) NOT NULL,
street varchar(40) NOT NULL,
house_nomber int NOT NULL,
flat_number int,
PRIMARY KEY(address_id)
);

INSERT INTO address(city, street, house_nomber, flat_number)
VALUES
('Kharkov',   'Sumskaya',       25,  10),
('Kuyiv',     'Khreschatic',    150, 25),
('Dnipro',    'Gagarina',       31,  176),
('Lviv',      'Simonenca',      12,  7),
('Zhytomyr',  'Pravdy',         15,  84),
('Rivne',     'Lysenca',        5,   7),
('Odessa',    'Privoz',         24,  37),
('Donetsk',   'Shahty',         13,  13),
('Mykolayiv', 'Portova',        27,  156),
('Chernihyv', 'Lisova',         45,  108),
('Zhytomyr',  'Pravdy',         24,  45),
('Zhytomyr',  'Lenina',         3,   7),
('Zhytomyr',  'Pravdy',         15,  12),
('Zhytomyr',  'Lenina',         15,  84),
('Rivne',     'Lysenca',        12,  44),
('Rivne',     'Shevchenka',     154, 256),
('Rivne',     'Lysenca',        5,   152),
('Rivne',     'Shevchenka',     24,  3),
('Rivne',     'Shevchenka',     154, 45),
('Kharkov',   'Sumskaya',       15,  7),
('Kharkov',   'Sumskaya',       45,  64),
('Kharkov',   'Ivanova',        25,  10),
('Kharkov',   'Ivanova',        10,  47),
('Kharkov',   'Ivanova',        167, 100),
('Odessa',    'Privoz',         24,  25),
('Odessa',    'Privoz',         45,  5),
('Odessa',    'Deribasovskaya', 3,   157),
('Odessa',    'Deribasovskaya', 3,   24),
('Odessa',    'Deribasovskaya', 15,  29),
('Donetsk',   'Shahty',         28,  75),
('Donetsk',   'Shahty',         13,  64),
('Donetsk',   'Gory',           157, 254),
('Donetsk',   'Gory',           25,  84);

SELECT * FROM address;

DROP TABLE student;

CREATE TABLE student(
student_id int AUTO_INCREMENT,
student_f_name varchar(30) NOT NULL,
student_l_name varchar(30) NOT NULL,
age int,
gender enum('male','female'),
birthday date,
address int,
university int,
faculty int,
groups int,
PRIMARY KEY (student_id),
FOREIGN KEY (university) REFERENCES university(university_id),
FOREIGN KEY (address) REFERENCES address(address_id),
FOREIGN KEY (faculty) REFERENCES faculty(faculty_id)
);


INSERT INTO student (student_f_name, student_l_name, age, gender, birthday, address, university, faculty, groups)
VALUES
('Serg',    'Udaltsov',    38, 'male',   '1978-10-18', 2, 1, 7, 1),
('Galya',   'Udaltsova',   33, 'female', '1984-07-19', 2, 2, 3, 5),
('Andrey',  'Valevsky',    33, 'male',   '1984-05-24', 1, 3, 5, 6),
('Rostik',  'Potrubach',   39, 'male',   '1978-11-23', 3, 1, 7, 2),
('Yana',    'Potrubach',   38, 'female', '1979-05-24', 3, 1, 7, 2),
('Sasha',   'Klymovych',   36, 'male',   '1981-05-26', 4, 5, 10, 10),
('Andrey',  'Antonenko',   39, 'male',   '1978-11-23', 5, 7, 6, 4),
('Natalya', 'Abramovich',  40, 'female', '1977-03-18', 6, 8, 9, 7),
('Olga',    'Ivanova',     25, 'female', '1992-07-18', 7, 9, 11, 11),
('Ivan',    'Grebenyuk',   36, 'male',   '1982-04-06', 10, 5, 10, 7),
('Slava',   'Akhmedov',    37, 'male',   '1981-09-27', 11, 6, 3, 4),
('Katya',   'Nikiforova',  22, 'female', '1995-11-12', 12, 13, 12, 15),
('Elena',   'Egoshina',    27, 'female', '1999-03-24', 13, 14, 17, 8),
('Galya',   'Atchintseva', 45, 'female', '1965-05-27', 14, 8, 2, 5),
('Katya1',  'Nikif',       22, 'female', '1995-11-12', 7, 6, 12, 10),
('Andrey',  'Sidorov',     24, 'male',   '1995-10-23', 15, 6, 5, 4),
('Sasha',   'Ivanenko',    30, 'male',   '1994-05-26', 14, 5, 5, 10),
('Tamara',  'Ivanova',     25, 'female', '1992-07-18', 15, 9, 7, 3),
('Elena',   'Tkachenko',   23, 'female', '2000-03-24', 16, 12, 9, 6),
('Natalya', 'Abramov',     40, 'female', '1977-03-18', 16, 8, 9, 12),
('Katya',   'Nikif',       22, 'female', '1995-11-12', 17, 3, 3, 3);

SELECT * FROM student;

SELECT student_f_name, student_l_name, faculty FROM student
INNER JOIN university
ON university_id = university
WHERE university.name = 'KHAI';

SELECT student_f_name, student_l_name FROM student
INNER JOIN university
ON university.university_id = student.university
WHERE university.name = 'KHAI' AND faculty = 7;

SELECT count(student_id) AS cnt, name FROM student
INNER JOIN university
ON university_id = university
GROUP BY name;

SELECT count(student_id) AS cnt, faculty_name FROM student
INNER JOIN faculty
ON faculty_id = faculty
GROUP BY faculty_name;

SELECT count(student_id) AS cnt, group_number FROM student 
INNER JOIN groups
ON group_id = groups
GROUP BY group_number
;


SELECT count(student_id) AS cnt, group_number FROM student
INNER JOIN groups
ON group_id = groups
GROUP BY group_number
ORDER BY cnt DESC LIMIT 1
;

SELECT count(student_id) AS cnt, street, house_nomber FROM student
INNER JOIN address
ON address_id = address
GROUP BY street, house_nomber
HAVING cnt > 1;

SELECT count(student_id) AS cnt, street, house_nomber FROM student
INNER JOIN address
ON address_id = address
WHERE street = 'Gagarina' AND house_nomber = 31;

SELECT date_format(birthday, '%d %b %y'), student_f_name, student_l_name FROM student;

SHOW TABLES;