package homeworks.data_base_immitation;

import java.util.*;

class RequestService {

    private List<Person> persons;

    RequestService() {
        this.persons = new ArrayList<>();
    }

    /**
     * This method adds new person to the data base;
     *
     * @param man is a Person parameter, which should be added to te DB.
     * */
    private void addNewPerson(Person man) {
        persons.add(man);
    }

    /**
     * This method fill the DB with the initial number of persons.
     * */
    void init() {

        addNewPerson(new Person("Sidorov", "Kolya", 28, 1,
                new Address(Country.RUSSIA, City.MOSKVA, "Marksa str.", 21, 45)));
        addNewPerson(new Person("Kernes", "Gena", 32, 3,
                new Address(Country.USA, City.KHARKIV, "Wall str.", 3, 7)));
        addNewPerson(new Person("Poroshenko", "Petro", 24, 4,
                new Address(Country.UKRAINE, City.KYIV, "Bankova str.", 37, 28)));
        addNewPerson(new Person("Trump", "Donald", 57, 5,
                new Address(Country.USA, City.NEW_YORK, "Wall str.", 3, 180)));
        addNewPerson(new Person("Trump", "Donald", 57, 5,
                new Address(Country.USA, City.NEW_YORK, "Wall str.", 3, 180)));
        addNewPerson(new Person("Atchintsev", "Vitaliy", 21, 0,
                new Address(Country.GERMANY, City.BERLIN, "Akhmetova str.", 3, 42)));
        addNewPerson(new Person("Clear", "Paul", 12, 0,
                new Address(Country.USA, City.DONETSK, "Wall str.", 42, 18)));
        addNewPerson(new Person("Kuraksin", "Artem", 34, 2,
                new Address(Country.USA, City.KHARKIV, "Wall str.", 17, 32)));
        addNewPerson(new Person("Holmes1", "Sherlock", 29, 1,
                new Address(Country.ENGLAND, City.LONDON, "Baker str.", 16, 3)));
        addNewPerson(new Person("Holmes2", "Sherlock", 29, 1,
                new Address(Country.ENGLAND, City.LONDON, "Baker str.", 16, 3)));
        addNewPerson(new Person("Holmes3", "Sherlock", 29, 1,
                new Address(Country.ENGLAND, City.LONDON, "Baker str.", 16, 3)));
        addNewPerson(new Person("Holmes4", "Sherlock", 29, 1,
                new Address(Country.UKRAINE, City.LONDON, "Baker str.", 16, 3)));
        addNewPerson(new Person("Holmes5", "Sherlock", 29, 1,
                new Address(Country.UKRAINE, City.LONDON, "Baker str.", 16, 3)));
        addNewPerson(new Person("Holmes6", "Sherlock", 29, 1,
                new Address(Country.UKRAINE, City.LONDON, "Baker str.", 16, 3)));
        addNewPerson(new Person("Holmes7", "Sherlock", 29, 1,
                new Address(Country.UKRAINE, City.LONDON, "Baker str.", 16, 3)));
        addNewPerson(new Person("Holmes8", "Sherlock", 29, 1,
                new Address(Country.ENGLAND, City.LONDON, "Baker str.", 16, 3)));
        addNewPerson(new Person("Watson", "Doctor", 19, 0,
                new Address(Country.SLOVAKIA, City.SLOVAKSK, "Stalina str.", 26, 34)));
    }

    /**
     * This method shows all the persons in DB.
     * */
    void showListOfPersons() {
        persons.forEach(System.out::println);
    }

    /**
     * This method shows complex information about the specified person.
     *
     * @param firstName is a String Parameter. It defines the first name of person which information will be shown.
     * @param lastName is a String Parameter. It defines the last name of person which information will be shown.
     * */
    void showPersonInfo(String firstName, String lastName) {
        persons.stream().filter(m -> m.getFirstName().equals(firstName) && m.getLastName().equals(lastName)).
                forEach(System.out::println);
    }

    /**
     * This method shows the address of every person in DB
     * */
    void showAdressOfAllPersons() {
        persons.forEach(m -> System.out.println(m.getAddress()));
    }


    void showInfoPersonsSortedByAgeAndFirstName() {
        Comparator<Person> comparator = (man1, man2) -> man1.getFirstName().compareTo(man2.getFirstName());
        persons.stream().filter(man -> man.getAge() >= 20).sorted(comparator).forEach(man ->
                System.out.println(man.getFirstName() + " " + man.getLastName() + ", has children - " +
                        man.getCountOfChildren()));
    }


    /**
     * This method changes first name, last name, and count of children for each person from USA.
     * */
    void changePersonInfo() {
        persons.stream().filter(man -> man.getAddress().getCountry() == Country.USA).forEach(man -> {
            man.setFirstName("John");
            man.setLastName("Kennedy");
            man.setCountOfChildren(3);
        });
    }

    /**
     * This method shows information of every person from USA and who have number of house 3 or age more than 25 y.o.
     * */
       void showPersonInfoSortedByCountryAgeAndHouseNumber() {
        persons.stream().filter(man -> (man.getAddress().getCountry() == Country.USA &&
                (man.getAddress().getNumberOfHouse() == 3) || man.getAge() >= 25)).forEach(man -> {

            System.out.println(man.getFirstName() + " " + man.getLastName() + " " + man.getAddress().getStreet());
        });
    }

    /**
     * This method shows all the persons who is from specified country and lives on a specified street.
     *
     * @param country is a Country instance param. It specifies a country for searching persons.
     * @param nameOfStreet is a String parameter. It specifies a street for searching persons.
     * */
    void showCountOfPersonsSortedByCountryAndStreet(Country country, String nameOfStreet) {
        long count = persons.stream().filter(man -> man.getAddress().getCountry() == country &&
                man.getAddress().getStreet().equals(nameOfStreet)).count();

        System.out.println("Count of suitable persons is " + count);
    }

    /**
     * This method counts how many persons from DB live in each street in specified country.
     *
     * @param country is a Country instance param. It specifies a country for searching persons
     * */
    void showCountOfCitizensOfStreetByCountry(Country country) {
        Set<String> streets = new HashSet<>();

        persons.forEach(man -> streets.add(man.getAddress().getStreet()));

        streets.forEach(s -> {
            int countOfManLivingOnStreet = (int) persons.stream().filter(man -> (man.getAddress().getStreet().equals(s)) &&
                    man.getAddress().getCountry() == country).count();

            if (countOfManLivingOnStreet > 3) {
                System.out.println("In country " + country + ", on street " + s + "live " +
                        countOfManLivingOnStreet + " citizens");
            }
        });
    }

    /**
     * This method shows count of persons living in the same county and street if their number is more than 3.
     * */
    void showCountOfPersonSortedByCountryAndStreet() {
        Map<String, Integer> addresses = new HashMap<>();

        persons.forEach(man -> {
            Address address = man.getAddress();
            String addressOfPerson = "In " + address.getCountry() + " on street " + address.getStreet() + " live ";
            int countOfLivingPersons = 1;

            if (addresses.containsKey(addressOfPerson)) {
                countOfLivingPersons += addresses.get(addressOfPerson);
            }
            addresses.put(addressOfPerson, countOfLivingPersons);
        });

        Iterator<String> iterator = addresses.keySet().iterator();

        while (iterator.hasNext()) {
            String key = iterator.next();

            if (addresses.get(key) > 3) {
                System.out.println(key + addresses.get(key) + "persons");
            }
        }
    }

}
