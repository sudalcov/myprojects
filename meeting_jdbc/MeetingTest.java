package homeworks.meeting_jdbc;

import java.sql.SQLException;

/**
 * This is an application Meeting Service based on JDBC
 *
 *
 * @author Udaltsov Sergey
 * @version 1.0
 * @since January 2018.
 * */

public class MeetingTest {
    public static void main(String[] args) throws SQLException {

        MeetingService service = new MeetingService();

        service.registerNewUser(new Person("Isaak", "Newtone", "male", 350, "London", 1));
        service.searchingByGender("male");
        service.findPerson("Sergey", "Esenin");
         service.smartFinder("Kyiv", "male", 23, 2);
        service.findPersonProcedure("Oleg", "Skrypka");
        service.deleteFromUsers(26);
        service.showPersonsInCities("Kharkov", "Kyiv", "Dnepr"); //IN clause
    }
}
