package homeworks.from_andrew.factory.stages;

import homeworks.from_andrew.factory.Stageable;

public class Leg implements Stageable {

    @Override
    public void action() throws InterruptedException {

        writeStage("Install legs");
        Thread.sleep(1000);
    }
}
