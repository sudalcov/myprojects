package homeworks.translator;

import java.io.IOException;
import java.util.Scanner;

/**
 * The TestTranslator program implements an application that translates the words.
 * This application permit to:
 * Add new languages
 * Add new words
 * Change languages
 * Bidirectional translating
 *
 * @author Udaltsov Sergey
 * @version 1.0
 * @since january 2018.
 */

public class TestTranslator {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        TranslatorService service = new TranslatorService();

        String word = "";

        service.startTranslator();

        showLanguageMenu();

        int clause = Integer.parseInt(getUserInfo("Choose the clause:"));

        String translatorName = null;

        switch (clause) {
            case 1: {
                translatorName = "Eng-Rus";
                break;
            }
            case 2: {
                translatorName = "Rus-Eng";
                break;
            }
            case 3: {
                translatorName = "De-Rus";
                break;
            }
            case 4: {
                translatorName = "Rus-De";
                break;
            }
        }


        while (!word.equalsIgnoreCase("exit")) {

            word = getUserInfo("Enter a word");

            service.translate(translatorName, word);
        }
    }


    private static String getUserInfo(String message) {
        System.out.println(message);
        return scanner.nextLine();
    }

    private static void showLanguageMenu() {
        System.out.println("1. English -> Russian\n" +
                "2. Russian -> English\n" +
                "3. German -> Russian\n" +
                "4. Russian -> German\n" +
                "5. Exit");
    }

}

