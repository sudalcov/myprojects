package homeworks.meeting;

public class MeetingService {
    private Person[] persons;
    private int addSizeOfPersons;

    public MeetingService(int countOfPersons) {
        this.persons = new Person[countOfPersons];
    }

    public void init() {
        persons[0]  = new Person(21, Gender.MALE,   City.DNEPR,     "Oleg Ivanov",        1);
        persons[1]  = new Person(25, Gender.MALE,   City.KYIV,      "Dmitrij Duzhev",     2);
        persons[2]  = new Person(23, Gender.MALE,   City.KYIV,      "Alexey Konev",       2);
        persons[3]  = new Person(20, Gender.FEMALE, City.KHARKOV,   "Galyna Udaltsova",   2);
        persons[4]  = new Person(17, Gender.FEMALE, City.ODESSA,    "Olga Samoilova",     0);
        persons[5]  = new Person(23, Gender.FEMALE, City.CHERKASSY, "Zinaida Nesterenko", 1);
        persons[6]  = new Person(22, Gender.MALE,   City.DNEPR,     "Oleg Krasnikov",     1);
        persons[7]  = new Person(27, Gender.MALE,   City.NIKOLAEV,  "Dmitrij Krasnikov",  2);
        persons[8]  = new Person(23, Gender.MALE,   City.KYIV,      "Alexey Drozd",       2);
        persons[9]  = new Person(29, Gender.FEMALE, City.KHARKOV,   "Galyna Atchintseva", 2);
        persons[10] = new Person(23, Gender.FEMALE, City.KHERSON,   "Olga Petrova",       0);
        persons[11] = new Person(23, Gender.FEMALE, City.CHERKASSY, "Zinaida Ivanova",    1);
        persons[12] = new Person(30, Gender.MALE,   City.DNEPR,     "Oleg Skrypka",       1);
        persons[13] = new Person(23, Gender.MALE,   City.KYIV,      "Dmitrij Kozlov",     2);
        persons[14] = new Person(29, Gender.MALE,   City.KYIV,      "Alexey Velichko",    2);
        persons[15] = new Person(23, Gender.FEMALE, City.KHARKOV,   "Galyna Potapova",    2);
        persons[16] = new Person(17, Gender.FEMALE, City.DONETSK,   "Olga Shevchenko",    0);
        persons[17] = new Person(20, Gender.FEMALE, City.CHERKASSY, "Zinaida Petrova",    1);
    }

    public void addPerson(Person person) {
        if (person.getAge() < 18) {
            System.out.println("Your age isn't allowed");
            return;
        }
        for (int i = 0; i < persons.length; i++) {
            if (persons[i] == null) {
                persons[i] = person;
                return;
            }
        }
        resize();
        persons[persons.length - addSizeOfPersons] = person;

        person.showGenderAndAge();

        showSuitablePersonsByAgeAndOppositeGender(person.getAge(), person.getGender());
    }


    private void showSuitablePersonsByAgeAndOppositeGender(int age, String gender) {
        for (Person person : persons) {

            if (person != null && person.getAge() == age && person.getGender() != gender) {
                System.out.println(person.getName() + " " + person.getGender() +
                        " " + person.getAge() + " y.o, from " + person.getCity() + " has " + person.getChildOfPerson() + " child.");

            }
        }
    }

    public void showListOfRegisteredPersons() {
        for (int i = 0; i < persons.length; i++) {
            Person person = persons[i];
            if (person == null) {
                return;
            }
            System.out.println(i + 1 + "  " +
                    person.getName()   + " " +
                    person.getGender() + " " +
                    person.getAge() + " y.o, from " +
                    person.getCity() + " has " +
                    person.getChildOfPerson() + " child.");

        }
        System.out.println();
    }

    public void showPersonsSuitableByGender(Person person) {
        System.out.println("\n Persons are suitable for" + person.getName() + "\n");
        for (int i = 0; i < persons.length; i++) {
            if (persons[i] == null) {
                return;
            }
            if (persons[i].getGender() != person.getGender()) {
                System.out.println(persons[i].getName() + " " + persons[i].getGender() +
                        " " + persons[i].getAge() + " y.o, from " + persons[i].getCity() + " has " + persons[i].getChildOfPerson() + " child.");

            }
        }
        System.out.println();
    }

    private void resize() {
        Person[] temp = new Person[(persons.length * 2)];
        for (int i = 0; i < persons.length; i++) {
            temp[i] = persons[i];
        }
        persons = temp;
    }

    public void showPersonByName(String nameOfPerson) {
        for (int i = 0; i < persons.length; i++) {
            if (persons[i] == null) {
                return;
            }
            if (persons[i].getName().equals(nameOfPerson)) {
                System.out.println(" \n Name - " +
                        persons[i].getName() + "\n Gender - " +
                        persons[i].getGender() + "\n Age - " +
                        persons[i].getAge() + "\n Live in city - "
                        + persons[i].getCity() + "\n Has child - " + persons[i].getChildOfPerson());
            }
        }
    }
    public void smartFinder(int age, Gender gender, City city, int childOfPerson){
        System.out.println("\n Persons who suit your parametries: \n");
        for (int i = 0; i < persons.length; i++) {
            if (persons[i] == null) {
                return;
            }
            if (persons[i].getGender() == gender.toString() &&
                    persons[i].getCity() == city &&
                    persons[i].getAge() == age &&
                    persons[i].getChildOfPerson() == childOfPerson) {
                System.out.println(persons[i].getName() + " " +
                        persons[i].getGender() + " " +
                        persons[i].getAge() +
                        " y.o, from " +
                        persons[i].getCity() +
                        " has " + persons[i].getChildOfPerson() +
                        " child.");

            }
        }
        System.out.println();

    }

}
