package homeworks.data_base_immitation;

/**
 * The TestRequest program implements an application that imitates work of SQL requests to database.
 * It based on framework Collections and lambda expressions.
 * This application provides ability to create entities Person and Address.
 *
 * Execute query:
 * "SELECT * FROM Person";
 * "SELECT * FROM Address";
 * "SELECT firstName, lastName, countOfChildren FROM Person WHERE age >= 20 ORDER BY firstName";
 * "UPDATE Person SET firstName = 'John', lastName = 'Kennedi', countOfChildren = 3 WHERE country == 'US' (or another country);
 * "SELECT firstName, lastName, nameOfStreet FROM Address WHERE country == 'Canada' AND numberOfHome == 3 OR age >= 25";
 * "SELECT count(*) FROM Address GROUP BY country, nameOfStreet";
 * "SELECT count(*) FROM Address GROUP BY country, nameOfStreet HAVING countOfCitizens > 10";
 * "DELETE FROM Person WHERE country = 'USA'";
 *
 *
 * @author Udaltsov Sergey
 * @version 1.0
 * @since december 2017.
 */
public class TestRequest {
    public static void main(String[] args) {
        RequestService service = new RequestService();

        service.init();
        service.showListOfPersons();
        service.showPersonInfo("Watson", "Doctor");
        service.showAdressOfAllPersons();
        service.showInfoPersonsSortedByAgeAndFirstName();
        service.changePersonInfo();
        service.showListOfPersons();
        service.showPersonInfoSortedByCountryAgeAndHouseNumber();
        service.showCountOfPersonsSortedByCountryAndStreet(Country.USA, "Wall str.");
        service.showCountOfCitizensOfStreetByCountry(Country.USA);
        service.showCountOfPersonSortedByCountryAndStreet();


    }

}
