package homeworks.ticket_office;

import java.util.ArrayList;
import java.util.List;

class TicketService {

    final int MAX_CAPACITY = 10;
    private static final List<Ticket> TICKETS = new ArrayList<>();


    void fulfillTicketOfice() {

        TICKETS.add(new Ticket());
        TICKETS.add(new Ticket());
        TICKETS.add(new Ticket());
        TICKETS.add(new Ticket());
        TICKETS.add(new Ticket());
        TICKETS.add(new Ticket());
        TICKETS.add(new Ticket());
        TICKETS.add(new Ticket());
        TICKETS.add(new Ticket());
        TICKETS.add(new Ticket());
    }

    List<Ticket> getTickets() {
        return TICKETS;
    }

    void buyTicket(int amount, String buyerName) {
        new Thread(new Buyer(amount), buyerName).start();
    }

    void returnTicket(int amount, String returnerName) {
        new Thread(new Returner(amount), returnerName).start();
    }
}

class Ticket{

}
