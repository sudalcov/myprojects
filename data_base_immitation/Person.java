package homeworks.data_base_immitation;

public class Person {
    private String firstName;
    private String lastName;
    private int age;
    private int countOfChildren;
    private Address address;

    public Person(String firstName, String lastName, int age, int countOfChildren, Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.countOfChildren = countOfChildren;
        this.address = address;
    }

    public Person() {

    }

    String getFirstName() {
        return firstName;
    }

    void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    String getLastName() {
        return lastName;
    }

    void setLastName(String lastName) {
        this.lastName = lastName;
    }

    int getAge() {
        return age;
    }

    int getCountOfChildren() {
        return countOfChildren;
    }

    void setCountOfChildren(int countOfChildren) {
        this.countOfChildren = countOfChildren;
    }

    Address getAddress() {
        return address;
    }


    @Override
    public String toString() {
        return firstName + " " + lastName + ", age= " + age +
                ", countOfChildren= " + countOfChildren + address;
    }
}
