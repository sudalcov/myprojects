package homeworks.meeting;

public class Person {
    private int age;
    private int childOfPerson;
    private String name;
    private City city;
    private Gender gender;

    public Person(int age, Gender gender, City city, String name, int childOfPerson) {
        this.age = age;
        this.gender  = gender;
        this.city = city;
        this.childOfPerson = childOfPerson;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public City getCity() {
        return city;
    }

    public String getGender() {
        return gender.toString();
    }

    public int getAge() {
        return age;
    }

    public int getChildOfPerson() {return childOfPerson;}


    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", child=" + childOfPerson +
                ", name='" + name + '\'' +
                ", city=" + city +
                ", gender=" + gender +
                '}';
    }

    public void showGenderAndAge() {
        System.out.println("The new added person is " + name +
                ". Persons are suitable for "         + age  +
                " by age and gender:\n");
    }
}
