package homeworks.calendar;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.zone.ZoneRulesException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

/**
 * This class is a service class for class TestCalendar .
 *
 * @author Udaltsov Sergey
 * @version 1.0
 * @since november 2017.
 */

public class CalendarService {
    private final static int WEEK = 7;
    private final static int MONTH = 1;
    private final static int YEAR = 1;
    private final static int DAYS_IN_YEAR = 365;

    private List<Event> events;
    private LocalDate date;
    private String text;

    public CalendarService() {
        events = new ArrayList<>();
    }

    public List<Event> getEvents() {
        return events;
    }

    /**
     * This method shows date and time in specified time-zone and all the events of this date.
     *
     * @param zoneId is a String parameter. It is a name of specified time-zone.
     */

    public void showDateInTimeZoneWithEvents(String zoneId) {
        LocalDateTime dateTimeInZoneId = LocalDateTime.now(ZoneId.of(zoneId));
        LocalDate dateInZoneId = dateTimeInZoneId.toLocalDate();

        System.out.println("Date and time in " + zoneId + " is " + dateTimeInZoneId.truncatedTo(ChronoUnit.MINUTES));

        for (Event event : events) {
            if (event.compareDates(dateInZoneId)) {
                System.out.print(event.getDate() + " " + event.getName() + "\n");
            }
        }

    }

    /**
     * This method creates new event on the specified date.
     * It throws an exception if a wrong format of date was passed.
     *
     * @param name is a String parameter. It is a name of event which will be added to the specified date.
     * @param date is a String parameter. It defines a date on which the new event will be added.
     */
    public void createNewEvent(String date, String name) throws FormatException {
        try {
            events.add(new Event(LocalDate.parse(date, DateTimeFormatter.ISO_DATE), name));

        } catch (NumberFormatException e) {

            throw new FormatException("Date format is incorrect", e);
        }
    }

    /**
     * This method shows all the events appointed on the specified date.
     * It throws an exception if a wrong format of date was passed.
     *
     * @param date is a String parameter. It defines a date for events to show.
     */
    public void showEventsThisDate(String date) throws FormatException {
        for (Event event : events) {
            try {
                if (event.compareDates(LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE))) {

                    System.out.print(event.getDate() + " - " + event.getName() + "\n");
                }
            } catch (NumberFormatException e) {

                throw new FormatException("Date format is incorrect", e);
            }
        }
        System.out.println();
    }


    /**
     * This method deletes every event from the specified date
     *
     * @param date is a String parameter. It defines a date on which all the events will be deleted.
     * */
    public void deleteEvent(String date) {
        Event tempEvent = null;
        for (Event event : events) {
            if (event.compareDates(LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE))) {
                tempEvent = event;
                break;
            }
        }
        events.remove(tempEvent);
    }

    /**
     * This method shows time and day of week in specified time-zone.
     * It throws an exception if the wrong format of zone-id was passed.
     *
     * @param zoneId is a String parameter. It defines a zone-id to show time and day of week.
     */
    public void showTimeAndDayOfWeekInSpecifiedTimeZone(String zoneId) {
        try {
            LocalDateTime now = LocalDateTime.now(ZoneId.of(zoneId));
            System.out.println("Time in your city is - " + now.toLocalTime().truncatedTo(ChronoUnit.MINUTES) + "\n" +
                    "Day of week in your city is - " + now.getDayOfWeek());
        } catch (ZoneRulesException e) {
            try {
                throw new ZoneException("Wrong format of ZoneID");
            } catch (ZoneException ex) {
                ex.printStackTrace();
            }

        }

    }

    /**
     * This method shows the date in a several periods(week, month, year)
     *
     * @param key is an int parameter. It defines a period wich will be chosen to show a date.
     * */
    public void showDatePlus(int key) {
        switch (key) {
            case 1: {
                text = "Date in a week - ";
                date = LocalDate.now().plusDays(WEEK);
                break;
            }
            case 2: {
                text = "Date in a month - ";
                date = LocalDate.now().plusMonths(MONTH);
                break;
            }
            case 3: {
                text = "Date in a year - ";
                date = LocalDate.now().plusYears(YEAR);
                break;
            }
        }
        System.out.println(text + date);

    }

    /**
     * This method show a summarising information about current date, such as
     * date, day of week, day of year, current time, and number of days until the New Year.
     *
     * */
    public void showDetailsOfToday() {

        LocalDateTime dateNow = LocalDateTime.now();

        System.out.println("Today is - " + dateNow.toLocalDate() + "\n" +
                "Today is - " + dateNow.getDayOfWeek() + "\n" +
                "Today is - " + dateNow.getDayOfYear() + " day of year" + "\n" +
                "Time now is - " + dateNow.toLocalTime().truncatedTo(ChronoUnit.MINUTES) + "\n" +
                "Days until New Year - " + (DAYS_IN_YEAR - dateNow.getDayOfYear()));


    }

    /**
     * This method shows current date in a specified format.
     * It throws an exception if a wrong format was passed.
     *
     * @param format id a String parameter. It defines a format in which current date will be shown.
     * */
    public void showDate(String format) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            System.out.println(LocalDateTime.now().format(formatter));
        } catch (IllegalArgumentException e) {
            System.out.println("You have entered a wrong format");
        } catch (InputMismatchException e) {
            System.out.println("You have entered a wrong format");
        }

    }

    /**
     * This is an informational method. It shows to user available formats of date parts.
     *
     * */
    void formatLettersInfo() {
        System.out.println("Use for format pattern such letters:" + "\n" +
                "y,u - year." + "\n" +
                "M - month." + "\n" +
                "d - day of month." + "\n" +
                "H - hour of day." + "\n" +
                "m - minute of hour." + "\n" +
                "s - second of minute.");
    }

}

