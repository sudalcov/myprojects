package homeworks.meeting_jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

class MeetingService {

    private static Connection connection;
    private static final String URL = "jdbc:mysql://localhost:3306/homeworks?user=root&password=123456";
    private List<Person> users;

    MeetingService() {
        this.users = new ArrayList<>();
    }

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Connection getConnection() {
        if (Objects.isNull(connection)) {
            try {
                connection = DriverManager.getConnection(URL);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    void showAllUsers() throws SQLException {

        try (Statement statement = getConnection().createStatement();

             ResultSet resultSet = statement.executeQuery("SELECT * FROM persons")) {

            showResults(resultSet);
        }
    }

    void smartFinder(String city, String gender, int age, int children) throws SQLException {

        String sql = "SELECT * FROM persons WHERE gender = ? AND age = ? AND city = ? AND count_of_children = ?";

        PreparedStatement statement = getConnection().prepareStatement(sql);

        statement.setString(1, gender);
        statement.setInt(2, age);
        statement.setString(3, city);
        statement.setInt(4, children);

        ResultSet resultSet = statement.executeQuery();

        showResults(resultSet);

    }

    void registerNewUser(Person person) throws SQLException {

        if (person.getAge() > 18) {

            String sqlInsert = "INSERT INTO persons (first_name, last_name, gender, age, city, count_of_children)" +
                    "VALUES(?, ?, ?, ?, ?, ?)";

            PreparedStatement preparedStatementInsert = getConnection().prepareStatement(sqlInsert);

            fillPreparedStatement(preparedStatementInsert, person);

            preparedStatementInsert.executeUpdate();

            String sql1Select = "SELECT * FROM persons WHERE age = ? AND gender NOT LIKE ?";

            PreparedStatement preparedStatementSelect = getConnection().prepareStatement(sql1Select);

            preparedStatementSelect.setInt(1, person.getAge());
            preparedStatementSelect.setString(2, person.getGender());

            ResultSet resultSet = preparedStatementSelect.executeQuery();

            showResults(resultSet);
        }
    }

    void findPerson(String firstName, String lastName) throws SQLException {
        String sql = "SELECT * FROM persons WHERE first_name = ? AND last_name = ?";

        PreparedStatement statement = getConnection().prepareStatement(sql);

        statement.setString(1, firstName);
        statement.setString(2, lastName);

        ResultSet resultSet = statement.executeQuery();

        showResults(resultSet);
    }

    void showPersonsInCities(String... array) throws SQLException {

        String sqlSource = "SELECT * FROM persons WHERE city IN(%s)";

        String join = String.join(",", Collections.nCopies(array.length, "?"));

        String query = String.format(sqlSource, join);

        PreparedStatement statement = getConnection().prepareStatement(query);

        IntStream.range(0, array.length).forEach(i -> {
            try {
                statement.setString(i + 1, array[i]);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        ResultSet resultSet = statement.executeQuery();

        showResults(resultSet);
    }

    void findPersonProcedure(String firstName, String lastName) throws SQLException {

        String callProcedure = "{CALL showPerson(?, ?, ?)}";

        String name = "name";

        CallableStatement statement = getConnection().prepareCall(callProcedure);

        statement.setString(1, firstName);
        statement.setString(2, lastName);
        statement.setString(3, name);

        statement.registerOutParameter(name, Types.VARCHAR);

        ResultSet resultSet = statement.executeQuery();

        showResults(resultSet);

        System.out.println(statement.getString(3));


    }

    void deleteFromUsers(int id) throws SQLException {

        String sql = "DELETE FROM persons WHERE id = ?";

        PreparedStatement statement = getConnection().prepareStatement(sql);
        statement.setInt(1, id);

        statement.executeUpdate();

    }

    void searchingByGender(String personGender) throws SQLException {

        String sql = "SELECT * FROM persons WHERE gender NOT LIKE ?";

        PreparedStatement preparedStatement = getConnection().prepareStatement(sql);

        preparedStatement.setString(1, personGender);

        ResultSet resultSet = preparedStatement.executeQuery();

        showResults(resultSet);
    }


    private void showResults(ResultSet resultSet) {

        users.clear();

        try {
            while (resultSet.next()) {
                String firstName = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");
                String gender = resultSet.getString("gender");
                int age = resultSet.getInt("age");
                String city = resultSet.getString("city");
                int children = resultSet.getInt("count_of_children");

                users.add(new Person(firstName, last_name, gender, age, city, children));
            }

            users.stream().forEach(System.out::println);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void fillPreparedStatement(PreparedStatement statement, Person person) throws SQLException {

        statement.setString(1, person.getFirstName());
        statement.setString(2, person.getLastName());
        statement.setString(3, person.getGender());
        statement.setInt(4, person.getAge());
        statement.setString(5, person.getCity());
        statement.setInt(6, person.getCountOfChildren());

    }
}
