package homeworks.advertisiment_agency;

import java.io.IOException;

public class FileExistException extends IOException {

    public FileExistException() {
        super();
    }

    public FileExistException(String message) {
        super(message);
    }
}
