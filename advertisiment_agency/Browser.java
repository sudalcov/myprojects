package homeworks.advertisiment_agency;

public enum Browser {
    MOZILLA("mozilla"),
    FIREFOX("firefox"),
    OPERA("opera"),
    INTERNET_EXPLORER("internet explorer"),
    SAFARY("safary");

    private String internetBrowserOfPlatform;

    Browser(String browser) {
        this.internetBrowserOfPlatform = browser;
    }

    @Override
    public String toString() {
        return internetBrowserOfPlatform;
    }
}
