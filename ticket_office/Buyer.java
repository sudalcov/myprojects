package homeworks.ticket_office;

import java.util.List;
import java.util.stream.IntStream;

public class Buyer implements Runnable {

    private TicketService service;
    private int amountOfTickets;

    Buyer(int amountOfTickets) {
        
        this.service = new TicketService();
        this.amountOfTickets = amountOfTickets;
    }

    @Override
    public void run() {

        try {
            buyTicket();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    private void buyTicket() throws InterruptedException {

        List<Ticket> ticketOffice = service.getTickets();

        synchronized (ticketOffice) {

            if (ticketOffice.size() >= amountOfTickets) {

                IntStream.rangeClosed(0, amountOfTickets - 1).forEach(i -> {

                    ticketOffice.remove(0);

                });

                System.out.println(amountOfTickets + " ticket(s) sold to " + Thread.currentThread().getName() +
                        "\n Tickets left " + ticketOffice.size());

                ticketOffice.notifyAll();

            } else {
                System.out.println("There's no enough tickets for you, please wait.");

                ticketOffice.wait();

                buyTicket();
            }
        }
    }
}
