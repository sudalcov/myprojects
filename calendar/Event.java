package homeworks.calendar;

import java.time.LocalDate;

public class Event {
    private LocalDate date;
    private String name;

    public Event(LocalDate date, String name) {
        this.date = date;
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public boolean compareDates(LocalDate newDate) {
        return date.equals(newDate);
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        boolean b = date != null ? !date.equals(event.date) : event.date != null;

        if (b) {
            return false;
        }

        return name != null ? name.equals(event.name) : event.name == null;
    }

}


