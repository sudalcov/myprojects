package homeworks.internet_shop;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class is a service class for class TestShop.
 *
 * @author Udaltsov Sergey
 * @version 1.0
 * @since november 2017.
 */
public class ServiceShop {

    private List<Product> products = new ArrayList<>();

    private List<User> users = new ArrayList<>();

    /**
     * This method adds a new Product to Internet shop
     *
     * @param name     is a String field. It is a name of Product.
     * @param article  is an article of new Product. It is a unique field for Products.
     * @param category is Enum. It switches to which category of Products belongs this Product. It makes us possible
     *                 to specify the associated Products.
     * @param price    is a price of Product. It is double.
     */
    public void addNewProduct(String name, int article, Categories category, double price) {

        products.add(new Product(name, article, category, price));
    }

    /**
     * This method deletes a specified Product from Internet shop. Searching Product which should be deleted
     * provides by article of Product.
     *
     * @param article is int argument for searching Product to delete.
     */
    public void deleteProduct(int article) {

        Product selectedProduct = products.stream().filter(m -> m.getArticle() == article).findFirst().get();

        products.remove(selectedProduct);
    }

    /**
     * This method adds a new User to Internet shop.
     *
     * @param name is a String field. It is a name of new User.
     * @param id   is an id of new User. It is an unique field for User.
     */
    public void addNewUser(String name, int id) {
        users.add(new User(name, id));
    }

    /**
     * This method adds a specified Product to basket of specified User.
     *
     * @param userId           is an id of User to which basket a Product will be added.
     * @param articleofproduct is an article of Product which will be added to User's basket.
     */
    public void addProductToUserBasket(int userId, int articleofproduct) {

        User selectedUser = users.stream().filter(m -> m.getUserId() == userId).findFirst().get();

        Product selectedProduct = products.stream().filter(m -> m.getArticle() == articleofproduct).findFirst().get();

        selectedUser.addProductToBasket(selectedProduct);

        Objects.hash(products, users);
    }

    /**
     * This method prints Product's info which are in basket of specified User.
     *
     * @param userId is a field for searching User.
     *               Also method calculates the sum of whole order of Products which are in basket.
     */
    public void showProductsInUserBasket(int userId) {

        User selectedUser = users.stream().filter(m -> m.getUserId() == userId).findFirst().get();

        selectedUser.showUserBasket();

        Calculatable calculatable = User::getSumProductsInBasket;

        System.out.println("The sum you should pay = " + calculatable.sum(selectedUser));
    }

    /**
     * This method prints the list of all Products which are presented in Internet shop.
     */
    public void showListOfProducts() {
        products.forEach(System.out::println);
    }

    /**
     * This method prints info of specified Product and also prints a list of associated Products to specified Product.
     *
     * @param nameofproduct is a String field for searching a Product needed to see info.
     */
    public void showProductInfoWithAssociatedProducts(String nameofproduct) {

        Product selectedProduct = products.stream().filter(m -> m.getName().equals(nameofproduct)).findFirst().get();

        System.out.println(selectedProduct + "\nAssociated products:");

        products.forEach(m -> {
            if (m.getCategory() == selectedProduct.getCategory() && m.getArticle() != selectedProduct.getArticle()) {
                System.out.println(m);
            }
        });
    }

    /**
     * This method prints info about all Users which are registered in Internet shop.
     */
    public void showListOfUsers() {
        users.forEach(System.out::println);
    }

    /**
     * This method is created for getting an object Product for class ServiceShopTest.
     */
    public Product getProduct(int articleofproduct) {
        return products.stream().filter(m -> m.getArticle() == articleofproduct).findFirst().get();
    }

    /**
     * This method is created for getting an object User for class ServiceShopTest.
     */
    public User getUser(int userId) {
        return users.stream().filter(m -> m.getUserId() == userId).findFirst().get();
    }

    /**
     * This is a functional interface for calculating the sum of order of Products in User's basket.
     */
    interface Calculatable {
        double sum(User user);
    }

}
