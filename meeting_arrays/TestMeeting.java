package homeworks.meeting;

/**
 * The TestMeeting program implements an application that imitates a work of meeting agency.
 * It provides possibilities to:
 * - register a new person with validation it's age.
 * - show suitable persons by age and gender for just registered person
 * - look through a list of registered persons. For men shows women and otherwise for women shows men
 * - look through a profile of a certain person
 * - manage a smart search(shows a list of persons suitable by age, gender, city, count of children)
 *
 * @author Udaltsov Sergey
 * @version 1.0
 * @since december 2017.
 */

public class TestMeeting {
    public static void main(String[] args) {

        MeetingService meetingService = new MeetingService(25);
        meetingService.init();
        meetingService.showListOfRegisteredPersons();
        meetingService.addPerson(new Person(23, Gender.MALE, City.KHARKOV, "Steeve", 2));

        Person personForLook = new Person(23, Gender.MALE, City.DONETSK, "Alex", 1);
        meetingService.showPersonsSuitableByGender(personForLook);

        meetingService.showPersonByName("Oleg Ivanov");

        meetingService.smartFinder(23, Gender.MALE, City.KYIV, 2);

    }
}
