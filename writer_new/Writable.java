package homeworks.writer_new;

import java.io.IOException;

public interface Writable {
    public abstract void write(String text) throws IOException;
}
