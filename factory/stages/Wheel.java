package homeworks.from_andrew.factory.stages;


import homeworks.from_andrew.factory.Stageable;

public class Wheel implements Stageable {

    @Override
    public void action() throws InterruptedException {

        writeStage("Install wheel");
        Thread.sleep(1000);
    }
}

