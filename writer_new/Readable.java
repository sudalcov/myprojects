package homeworks.writer_new;

import java.io.IOException;
import java.nio.file.Path;

public interface Readable {

    abstract String read(Path path) throws IOException;
}
