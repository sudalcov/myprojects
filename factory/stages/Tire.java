package homeworks.from_andrew.factory.stages;

import homeworks.from_andrew.factory.Stageable;

public class Tire implements Stageable {

    @Override
    public void action() throws InterruptedException {

        writeStage("Install tire");
        Thread.sleep(1000);
    }
}
