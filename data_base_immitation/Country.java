package homeworks.data_base_immitation;

public enum Country {
    UKRAINE("Ukraine"),
    RUSSIA("Russia"),
    SLOVAKIA("Slovakia"),
    GERMANY("Germany"),
    ENGLAND("England"),
    USA("Usa");

    private String country;

    Country(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "country - " + country;
    }
}
