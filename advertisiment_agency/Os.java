package homeworks.advertisiment_agency;

public enum Os {
    WINDOWS("windows"),
    MAC_OS("mac operationSystem"),
    LINUX("linux"),
    IOS("ios");

    private String operationSystem;

    Os(String os) {
        this.operationSystem = os;
    }

    @Override
    public String toString() {
        return operationSystem;
    }
}
