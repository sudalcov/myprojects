package service;

import domain.*;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.List;


@NamedQueries({

        @NamedQuery(
                name = "Opposite gender",
                query = "SELECT c FROM Person c WHERE c.gender != :gender"
        ),

        @NamedQuery(
                name = "name",
                query = "SELECT c FROM Person c WHERE c.name = :name"
        )


})
public class MeetingService {

    private static final EntityManagerFactory factory = Persistence.createEntityManagerFactory("meeting");

    private static final ThreadLocal<EntityManager> tl = new ThreadLocal<EntityManager>();

    private static EntityManager getEntityManager() {
        EntityManager em = tl.get();

        if (em == null) {
            em = factory.createEntityManager();
            tl.set(em);
        }
        return em;
    }


    public void initDataBase() {

        EntityManager em = getEntityManager();

        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.persist(new Person("Oleg Ivanov", City.DNEPR, 3, Gender.MALE, 23, 2));
        em.persist(new Person("Dmitrij Duzhev", City.KYIV, 2, Gender.MALE, 25, 2));
        em.persist(new Person("Alexey Konev", City.KYIV, 2, Gender.MALE, 23, 2));
        em.persist(new Person("Alexey Drozd", City.KYIV, 2, Gender.MALE, 23, 2));
        em.persist(new Person("Dmitrij Kozlov", City.KYIV, 2, Gender.MALE, 23, 2));
        em.persist(new Person("Alexey Velichko", City.KYIV, 2, Gender.MALE, 29, 2));
        em.persist(new Person("Oleg Krasnikov", City.DNEPR, 3, Gender.MALE, 22, 1));
        em.persist(new Person("Oleg Skrypka", City.DNEPR, 3, Gender.MALE, 30, 1));
        em.persist(new Person("Dmitrij Krasnikov", City.NIKOLAEV, 6, Gender.MALE, 27, 2));
        em.persist(new Person("Galyna Udaltsova", City.KHARKIV, 1, Gender.FEMALE, 20, 2));
        em.persist(new Person("Galyna Potapova", City.KHARKIV, 1, Gender.FEMALE, 23, 2));
        em.persist(new Person("Galyna Atchintseva", City.KHARKIV, 1, Gender.FEMALE, 29, 1));
        em.persist(new Person("Olga Samoilova", City.ODESSA, 4, Gender.FEMALE, 17, 0));
        em.persist(new Person("Zinaida Nesterenko", City.CHERKASSY, 5, Gender.FEMALE, 23, 1));
        em.persist(new Person("Zinaida Ivanova", City.CHERKASSY, 5, Gender.FEMALE, 23, 1));
        em.persist(new Person("Zinaida Petrova", City.CHERKASSY, 5, Gender.FEMALE, 20, 1));
        em.persist(new Person("Olga Shevchenko", City.DONETSK, 8, Gender.FEMALE, 17, 0));
        em.persist(new Person("Olga Petrova", City.KHERSON, 7, Gender.FEMALE, 23, 0));

        transaction.commit();


    }

    public void registerNewUser(Person person) {

        EntityManager em = getEntityManager();

        if (person.getAge() > 18) {

            EntityTransaction transaction = em.getTransaction();

            transaction.begin();

            em.persist(person);

            // showUsersByAge(person.getAge(), person.getGender());

            showUsersByAgeCriteria(person.getAge(), person.getGender(), em);

            transaction.commit();

        }
    }


    public void showUsersByGender(Gender gender) {

        EntityManager em = getEntityManager();

        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        Query query = em.createNamedQuery("Opposite gender");

        query.setParameter("gender", gender);

        List<Person> results = (List<Person>) query.getResultList();

        results.stream().forEach(System.out::println);
        transaction.commit();
    }

    void showUserByName(String name) {

        EntityManager em = getEntityManager();

        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        Query namedQuery = em.createNamedQuery("name");

        namedQuery.setParameter("name", name);

        List<Person> results = (List<Person>) namedQuery.getResultList();

        results.stream().forEach(System.out::println);

        transaction.commit();

    }


    void showUsersByParams(int age, City city, Gender gender) {

        EntityManager em = getEntityManager();

        String sql = "SELECT c FROM Person c WHERE c.age = :age AND c.city = :city AND c.gender = :gender";

        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        Query query = em.createQuery(sql);

        query.setParameter("age", age);
        query.setParameter("city", city);
        query.setParameter("gender", gender);

        List<Person> results = (List<Person>) query.getResultList();

        results.stream().forEach(System.out::println);

        transaction.commit();

    }

    void changeAge(int oldAge, int newAge) {

        EntityManager em = getEntityManager();

        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        String sql = "UPDATE Person c SET c.age = :new_age WHERE c.age = :old_age";

        Query query = em.createQuery(sql);

        query.setParameter("new_age", newAge);
        query.setParameter("old_age", oldAge);

        query.executeUpdate();
        transaction.commit();
    }


    void showUsersByAge(int age, Gender gender) {

        EntityManager em = getEntityManager();

        String sql = "SELECT c FROM Person c WHERE c.age = :age AND c.gender != :gender";

        Query query = em.createQuery(sql);

        query.setParameter("age", age);
        query.setParameter("gender", gender);

        List<Person> resultList = (List<Person>) query.getResultList();

        resultList.stream().forEach(System.out::println);

    }

    public void showAllUsersCriteria() {

        EntityManager em = getEntityManager();

        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<Person> query = builder.createQuery(Person.class);

        Root<Person> root = query.from(Person.class);

        query.select(root);

        TypedQuery<Person> typedQuery = em.createQuery(query);

        typedQuery.getResultList().stream().forEach(System.out::println);

    }


    void showUsersByAgeCriteria(int age, Gender gender, EntityManager em) {

        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<Person> query = builder.createQuery(Person.class);

        Root<Person> root = query.from(Person.class);

        Path<Integer> personAge = root.get(Person_.age);

        Path<Gender> personGender = root.get(Person_.gender);

        query.where(builder.equal(personAge, age), builder.notEqual(personGender, gender));

        TypedQuery<Person> typedQuery = em.createQuery(query);

        typedQuery.getResultList().stream().forEach(System.out::println);
    }

    public void showUsersByGenderCriteria(Gender gender) {

        EntityManager em = getEntityManager();

        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<Person> query = builder.createQuery(Person.class);

        Root<Person> root = query.from(Person.class);

        Path<Gender> personGender = root.get(Person_.gender);

        query.where(builder.notEqual(personGender, gender));

        TypedQuery<Person> typedQuery = em.createQuery(query);

        typedQuery.getResultList().stream().forEach(System.out::println);

        transaction.commit();
    }

    public void showUserByNameCriteria(String name) {

        EntityManager em = getEntityManager();

        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<Person> query = builder.createQuery(Person.class);

        Root<Person> root = query.from(Person.class);

        Path<String> personName = root.get(Person_.name);

        query.where(builder.equal(personName, name));

        TypedQuery<Person> typedQuery = em.createQuery(query);

        typedQuery.getResultList().stream().forEach(System.out::println);

        transaction.commit();

    }


    public void findPersonByParamCriteria(int age, Gender gender, City city) {

        EntityManager em = getEntityManager();

        EntityTransaction tr = em.getTransaction();

        tr.begin();

        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<Person> query = builder.createQuery(Person.class);

        Root<Person> root = query.from(Person.class);

        Path<Integer> personAge = root.get(Person_.age);

        Path<Gender> personGender = root.get(Person_.gender);

        Path<City> personCity = root.get(Person_.city);

        query.where(builder.equal(personAge, age), builder.equal(personGender, gender), builder.equal(personCity, city));

        TypedQuery<Person> typedQuery = em.createQuery(query);

        typedQuery.getResultList().stream().forEach(System.out::println);

        tr.commit();

    }

    public void closeConnection() {
        getEntityManager().close();
    }


}
