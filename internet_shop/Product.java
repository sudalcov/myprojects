package homeworks.internet_shop;

public class Product {
    private String name;
    private Double price;
    private int article;
    private int amount;
    private String manufacturer;
    private Categories category;

   public Product(String name, int article, Categories category, double price) {
        this.category = category;
        this.name = name;
        this.article = article;
        this.price = price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public int getArticle() {
        return article;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", article=" + article +
                '}';
    }

    public void toStringSimple() {
        System.out.println("prod  " + name + ", art " + article);
    }

    Categories getCategory(){
        return category;
    }

}
