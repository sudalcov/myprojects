package homeworks.from_andrew.factory.stages;

import homeworks.from_andrew.factory.Stageable;

public class Frame implements Stageable {

    @Override
    public void action() throws InterruptedException {

        writeStage("Install frame");
        Thread.sleep(1000);
    }
}
