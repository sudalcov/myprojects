package homeworks.writer_new;

import java.io.IOException;
import java.nio.file.Path;

class ChildReader extends Reader {

    private Path path;

    ChildReader(Path path) {
        this.path = path;
    }

    void modifyText() throws IOException {

        String text = read(path);

        String newText = text.replace("I'm ready for writing to file", "I'm from file");

        System.out.println(newText);

    }

}
