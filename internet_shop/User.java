package homeworks.internet_shop;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class User {
    private String name;
    private int userId;
    private List<Product> basket;

    User(String name, int id_number) {
        this.userId = id_number;
        this.name = name;
        this.basket = new ArrayList<>();
    }


    int getUserId() {
        return userId;
    }

    public List<Product> getBasket() {
        return basket;
    }

    void addProductToBasket(Product product) {
        basket.add(product);
    }

    public String getName() {
        return name;
    }

    public void showUserBasket() {
        System.out.println("Basket of user " + name + " contains products:");
        basket.forEach(System.out::println);
    }

    public double getSumProductsInBasket() {
        return  basket.stream().collect(Collectors.summingDouble(Product::getPrice));
    }


    @Override
    public String toString() {
        return "User name= " + name + ", userId=" + userId;
    }
}
