package homeworks.ticket_office;

import java.util.Scanner;

/**
 * The TestOffice program implements an application that imitates work of ticket office.
 *
 * At first there are 10 tickets in the office.
 * There are buyers and those who want to return tickets.
 * If any buyer buys a ticket or more, the application prints who bought and how many tickets were bought.
 * Also it print how many tickets left in the office. If any buyer prompts to buy more tickets than there are in the
 * office, tha application prints that tha buyer have to wait while any ticket will be returned. When returner returns
 * tickets and it is enough for the buyer who was waiting to buy them, the application automatically sells them
 * to this buyer.
 * Every buyer and returner is a different thread.
 *
 *
 * @author Udaltsov Sergey
 * @version 1.0
 * @since december 2017.
 */

public class TestOffice {

    static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) throws InterruptedException {

        TicketService service = new TicketService();

        service.fulfillTicketOfice();

        while (true) {

            menu();

            try {

                int key = Integer.parseInt(getUserInfo("Enter a number of action"));

                switch (key) {
                    case 1: {

                        int amountOfTicket =
                                Integer.parseInt(getUserInfo("Enter an amount of tickets you need"));

                        String buyerName = getUserInfo("Enter your name");

                        service.buyTicket(amountOfTicket, buyerName);

                        Thread.sleep(1000);

                        break;
                    }

                    case 2: {

                        int amountOfTicket =
                                Integer.parseInt(getUserInfo("Enter an amount of tickets you want to return"));

                        String returnerName = getUserInfo("Enter your name");

                        service.returnTicket(amountOfTicket, returnerName);

                        Thread.sleep(1000);

                        break;
                    }

                    case 3: {
                        return;
                    }

                    default: {
                        System.out.println("You've entered a wrong number");
                    }
                }
            } catch (NumberFormatException e) {

                System.out.println("You've entered not a number, try again please");
            }
        }


    }


    private static void menu() {
        System.out.println("1. Buy ticket \n" +
                "2. Return ticket \n" +
                "3. Exit");
    }

    private static String getUserInfo(String message) {
        System.out.println(message);
        return SCANNER.nextLine();
    }
}
