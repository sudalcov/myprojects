USE homeworks;

CREATE TABLE product(
product_id int AUTO_INCREMENT,
product_name varchar(50) UNIQUE,
manufacture_date date,
producer int,
strength enum('fragile', 'strong'),
price double,
PRIMARY KEY (product_id),
FOREIGN KEY (producer) REFERENCES producer(producer_id)
#UNIQUE_KEY `product_name - producer` ()
);

CREATE TABLE producer(
producer_id int AUTO_INCREMENT,
brand varchar(50),
country varchar(25),
PRIMARY KEY (producer_id)
);

INSERT INTO producer(brand, country) VALUES
("Sony", "Japan"),
("Samsung", "Korea"),
("BMW", "Germany"),
("Lamborgini", "Italy");

INSERT INTO product(product_name, manufacture_date, producer, strength, price) VALUES
("Telephone",  "2012.8.15", 2, "fragile", 2500),
("TV set",     "2017.5.25", 1, "fragile", 10000),
("telephon",   "2015.3.12", 1, "fragile", 1500),
("notebook", "2013.9.5", 2, "fragile", 7000),
("x5", "2013.6.13", 3, "strong", 150000),
("m3", "2015.7.21", 3, "strong", 120000),
("galardo", "2017.6.26", 4, "strong", 350000),
("diablo", "2016.1.10", 4, "strong", 400000);

DROP TABLE product;
DROP TABLE producer;

SELECT * FROM product WHERE producer = 1 AND strength = "fragile";

SELECT * FROM product WHERE manufacture_date BETWEEN "2013.01.01" AND "2017.01.01";

SELECT * FROM product WHERE price BETWEEN 10000 AND 200000;


SELECT * FROM product WHERE product_name LIKE 't%';

SELECT * FROM product WHERE product_name LIKE 'tel%';

SELECT * FROM product ORDER BY manufacture_date ASC LIMIT 5;

SELECT * FROM product ORDER BY manufacture_date DESC LIMIT 5;

SELECT concat(p.product_name, " -> ", p.price, "$, ",pd.brand, " # ", pd.country) 
FROM product AS p, producer AS pd WHERE p.product_id = 3 AND pd.producer_id = p.producer;


SELECT * FROM product;
SELECT * FROM producer;


CREATE TABLE persons(
person_id int AUTO_INCREMENT,
name varchar(20),
birthday date,
PRIMARY KEY (person_id)
);

DROP TABLE persons;

INSERT INTO persons(name, birthday)
VALUES("vaDim", "2001.09.12"),
("serG", "1979.10.18"),
("gaLya", "1984.07.19"),
("KiriLl", "2007.01.10");

SELECT * FROM persons;

UPDATE persons SET name = concat(upper(substring(name, 1, 1)), lower(mid(name, 2)));

DROP TABLE persons;

CREATE TABLE employee(
employee_id int AUTO_INCREMENT,
f_name varchar(20),
l_name varchar(20),
department_id int,
PRIMARY KEY (employee_id),
FOREIGN KEY (department_id) REFERENCES department(department_id)
);

CREATE TABLE department(
department_id int AUTO_INCREMENT,
city varchar(20),
PRIMARY KEY (department_id)
);

INSERT INTO department(city)
VALUES
("Lviv"),
("Dnepr"),
("Kharkiv"),
("Kuyv"),
("Lugansk");

INSERT INTO employee(f_name, l_name, department_id)
VALUES
("Serg", "Udaltsov", 1),
("Vadim", "Udaltsov", 2),
("Andrey", "Valevsky", 2),
("Rostik", "Potrubach", 3),
("Sasha", "Klymovich", 2),
("Ivan", "Grebenyuk", 4),
("Slava", "Akhmedov", 3),
("Ivan", "Yurchenko", 4),
("Sergey", "Valevsky", 3),
("Peter", "Grebenyuk", 3),
("Serg", "uhuuh", 1),
("Serg", "Udaltsov", 1),
("Serg", "rdryhijok", 1),
("Serg", "uhgyftf", 1),
("Serg", "rdsrdtgu", 1),
("Andrey", "yftfjko", 2),
("Andrey", "yftfopiuyu", 2),
("Andrey", "yftfvtg", 2),
("Andrey", "yftfhgvgvjij", 2),
("Andrey", "hvgyyftfjij", 2),
("ygftft", "uygyg", NULL)
;



SELECT city FROM department;
SELECT DISTINCT f_name FROM employee;
SELECT count(f_name) FROM employee WHERE department_id = 2;

SELECT department_id, count(employee_id) AS empl FROM employee GROUP BY department_id ORDER BY empl DESC;
SELECT * FROM department WHERE city LIKE 'l%';
SELECT f_name, l_name, count(employee_id) AS cnt FROM employee GROUP BY l_name HAVING cnt > 1 ORDER BY l_name ASC; 

SELECT count(employee_id), department.department_id FROM employee
INNER JOIN department
ON department.department_id = employee.department_id
GROUP BY city
ORDER BY city DESC;

SELECT count(employee_id), city FROM employee
INNER JOIN department
ON department.department_id = employee.department_id
WHERE city LIKE '%l%'
GROUP BY city
ORDER BY city ASC;

SELECT city, count(employee_id) FROM department
LEFT JOIN employee
ON employee.department_id = department.department_id
WHERE city LIKE '%l%'
GROUP BY city;


SELECT count(f_name) AS cnt, l_name, city FROM employee
INNER JOIN department
ON department.department_id = employee.department_id
WHERE city = 'Lviv'
GROUP BY f_name, l_name
HAVING cnt > 1
ORDER BY f_name ASC

SELECT count(f_name) AS cnt, city FROM employee
INNER JOIN department
ON department.department_id = employee.department_id
GROUP BY city
HAVING cnt > 4
ORDER BY employee_id DESC;


SELECT * FROM department;
SELECT * FROM employee;



DROP TABLE employee;
DROP TABLE department;




