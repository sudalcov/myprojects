package homeworks.translator;

import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.FileVisitResult;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

class TranslatorService {

    private static final String START_DIRECTORY = "./Translator";
    private static final Path START_DIRECTORY_PATH = Paths.get(START_DIRECTORY);

    private Map<String, Map<String, String>> translators;

    TranslatorService() {

        this.translators = new HashMap<>();
    }


    void startTranslator() throws IOException {

        Files.walkFileTree(START_DIRECTORY_PATH, new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

                createTranslator(file);

                return FileVisitResult.CONTINUE;
            }
        });
    }


    void translate(String translatorName, String word) {

        Map<String, String> translator = translators.get(translatorName);

        String translatedWord = translator.get(word);

        if (Objects.isNull(translatedWord)) {

            translatedWord = "Sorry, we have no translation for this word";
        }

        System.out.println(translatedWord);
    }


    private void createTranslator(Path pathToFile) {

        String fileName = pathToFile.toFile().getName();

        int positionDefis = fileName.indexOf('-');

        int positionPoint = fileName.indexOf('.');

        String translatorName = fileName.substring(0, positionPoint);

        String reverseTranslatorName = fileName.substring(positionDefis + 1, positionPoint) + "-" +
                                       fileName.substring(0,positionDefis);


        Map<String, String> translator = new HashMap<>();

        Map<String, String> translatorReverse = new HashMap<>();

        try {

            List<String> pairs = Files.readAllLines(pathToFile);

            pairs.stream().forEach(t -> {

                String[] pair = t.split(" ");

                translator.put(pair[0], pair[1]);
                translatorReverse.put(pair[1], pair[0]);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        translators.put(translatorName, translator);
        translators.put(reverseTranslatorName, translatorReverse);
    }

}
