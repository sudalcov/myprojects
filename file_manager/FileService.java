package homeworks.file_manager;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * This is a service class for TestFileManager class.
 */

public class FileService {

    public static final String CURRENT_DIR = "./";
    public static final String NEW_FOLDER = CURRENT_DIR + "New folder";
    public static final Path PATH_TO_NEW_FOLDER = Paths.get(NEW_FOLDER);
    private Path pathToAnotherNewDir;
    private int counter = 1;


    /**
     * This method creates a new folder in a root directory.
     * It checks if the specified directory already exists.
     * If it exists the method adds a number to a directory name and creates it
     */
    public void createNewDirectory() throws IOException {
        if (Files.notExists(PATH_TO_NEW_FOLDER)) {
            Files.createDirectory(PATH_TO_NEW_FOLDER);
            return;
        }
        while (Files.exists(PATH_TO_NEW_FOLDER)) {
            pathToAnotherNewDir = Paths.get(NEW_FOLDER + "(" + counter + ")");
            counter++;
        }
        Files.createDirectory(pathToAnotherNewDir);
    }

    /**
     * This method creates a new file with a specified name.
     * It checks if the specified file already exists.
     * If it exists the method prints a message about it and doesn't create a file.
     *
     * @param fileName is a String parameter. It specifies the name of file to create.
     */
    public void createNewFile(String fileName) throws IOException {

        Path pathToFile = Paths.get(CURRENT_DIR + fileName);

        if (Files.notExists(pathToFile)) {
            Files.createFile(pathToFile);
            return;
        }
        System.err.println("File " + pathToFile.toFile() + "  is already exists");
    }

    /**
     * This method copies a file into specified position.
     * It checks if the specified file already exists.
     * If it doesn't exist the method prints a message about it and doesn't do anything.
     *
     * @param nameOfFile is a String parameter. It specifies a name of file to copy.
     * @param nameOfNewFilePosition is a String parameter. It specifies the new position of copied file.
     */
    public void copy(String nameOfFile, String nameOfNewFilePosition) throws IOException {

        Path pathToFile = Paths.get(FileService.CURRENT_DIR + nameOfFile);
        Path pathToNewFilePosition = Paths.get(FileService.CURRENT_DIR + nameOfNewFilePosition);

        if (Files.exists(pathToFile)) {
            try {
                Files.copy(pathToFile, pathToNewFilePosition, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        System.err.println("File " + pathToFile.toFile() + " not found.");
    }

    /**
     * This method moves specified file to new specified position.
     * It can also be used for renaming files.
     *
     * @param nameOfOldFile is a String parameter. It specifies a name of file to rename.
     * @param nameOfNewFile is a String parameter. It specifies the new position of moved file.
     */
    public void renameOrMove(String nameOfOldFile, String nameOfNewFile) {

        Path pathToOldFile = Paths.get(CURRENT_DIR + nameOfOldFile);
        Path pathToNewFile = Paths.get(CURRENT_DIR + nameOfNewFile);

        if (Files.exists(pathToOldFile) && !Files.exists(pathToNewFile)) {
            try {
                Files.move(pathToOldFile, pathToNewFile, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        System.err.println(pathToOldFile.toFile() + " not found or " + pathToNewFile.toFile() + " already exists.");
    }


    /**
     * This method deletes file or folder.
     * If the directory needs to be deleted it deletes all the files within it and then deletes a directory.
     *
     * @param nameOfFile is a String parameter. It specifies the name of file or directory which should be deleted.
     * */
    public void delete(String nameOfFile) {

        Path pathToFile = Paths.get(FileService.CURRENT_DIR + nameOfFile);

        if (Files.isDirectory(pathToFile)) {
            MyFileVisitor visitor = new MyFileVisitor();
            try {
                Files.walkFileTree(pathToFile, visitor);
                Files.delete(pathToFile);
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            if (Files.exists(pathToFile)) {
                Files.delete(pathToFile);
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.err.println(pathToFile.toFile() + " not found.");
    }

    /**
     * This method shows a list of files stored in specified directory.
     *
     * @param directoryName is a String parameter. It specifies the name of directory wich list of files should be shown.
     * */
    public void showListOfFiles(String directoryName) {

        Path pathToDirectory = Paths.get(FileService.CURRENT_DIR + directoryName);

        File file = new File(CURRENT_DIR + directoryName);

        if (Files.isDirectory(pathToDirectory)) {
            String[] names = file.list();
            for (String name : names) {
                System.out.println(name);
            }
            return;
        }
        System.err.println(directoryName + " is not a directory.");
    }

    /**
     * This method converts a specified file into a PDF format.
     * It uses a library itextpdf-5.4.1.
     *
     * @param nameOfFile is a String parameter. It specifies the name of file which should be converted.
     * */
    public void convertToPdf(String nameOfFile) throws IOException, DocumentException {

        Path pathToFile = Paths.get(FileService.CURRENT_DIR + nameOfFile);

        BufferedReader reader = Files.newBufferedReader(pathToFile);

        Document document = new Document();

        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("./TestFileServiceConvert.pdf"));

        document.open();

        while (reader.ready()) {
            document.add(new Paragraph(reader.readLine()));
        }

        document.close();
        writer.close();
        reader.close();
    }

}
