
DELIMITER // 

CREATE PROCEDURE getAllProducts()
BEGIN
	SELECT * FROM product;
END

DELIMITER ;

CALL getAllProducts();

SET @first_id = 'Hello';

SET @first1_id = 3;

SELECT @first_id;

SELECT @second_id;

SELECT * FROM man WHERE man_id BETWEEN @first_id AND @first1_id;

SET @my_variable := (SELECT man_id FROM man WHERE man_id = 2);

SELECT @my_variable;


/*
 * 
 * 	An IN parameter passes a value into a procedure. 
	The procedure might modify the value, but the modification is not visible to the caller 
	when the procedure returns. 
	
	An OUT parameter passes a value from the procedure back to 
	the caller. Its initial value is NULL within the procedure, and its value is visible to 
	the caller when the procedure returns.

	An INOUT parameter is initialized by the caller, 
	can be modified by the procedure, and any change made by the procedure is visible to the 
	caller when the procedure returns.*/

CREATE PROCEDURE getOneMen(IN m_id INT, OUT first_name varchar(50), INOUT last_name varchar(50))
BEGIN

	SELECT * FROM man m WHERE m.man_id = m_id OR m.first_name = first_name OR m.last_name = last_name;

	
	SET m_id = 55;

	SET first_name = 'favalev';

	SET last_name = 'lavalev';
END

SET @new_m_id = 22;

SET @new_first_name = 'fav';

SET @new_last_name = 'lav';


CALL getOneMen(@new_m_id, @new_first_name, @new_last_name);

SELECT @new_m_id;

SELECT @new_first_name;

SELECT @new_last_name;


CREATE PROCEDURE getAllMenWithIf_ELSEStatement(OUT start_id int(11))

BEGIN
	
	IF start_id <= 10
	THEN SET start_id = 44;
	ELSEIF (start_id > 10 AND start_id <= 15)
	THEN SET start_id = 99;
END IF;

END 

SET @start_id = 9;

CALL getAllMenWithIf_ELSEStatement(@start_id);

SELECT @start_id;


