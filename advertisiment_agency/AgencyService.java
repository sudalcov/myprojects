package homeworks.advertisiment_agency;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class AgencyService implements Serializable {

    private List<Platform> platforms;
    private static final Scanner SCANNER = new Scanner(new InputStreamReader(System.in));
    private int countOfExistingPlatform = 0;

    public AgencyService() {
        this.platforms = new ArrayList<>();
    }

    public void createNewPlatform(String nameOfPlatform, Browser browser, Os os, int countOfDisplays) {

        Path pathToNewPlatform = Paths.get(ConstantsSummary.START_DIR + nameOfPlatform);

        if (Files.notExists(pathToNewPlatform)) {

            Platform platform = new Platform(nameOfPlatform, browser, os, countOfDisplays);
            platforms.add(platform);
            return;
        }
        nameOfPlatform = nameOfPlatform + "(" + countOfExistingPlatform++ + ")";

        createNewPlatform(nameOfPlatform, browser, os, countOfDisplays);
    }

    public void deletePlatform(String nameOfPlatform) {

        Path pathToDirectory = Paths.get(ConstantsSummary.START_DIR + nameOfPlatform);

        try {
            Files.walkFileTree(pathToDirectory, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    return delete(dir);
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    return delete(file);
                }

                private FileVisitResult delete(Path file) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }
            });

        } catch (IOException e) {
            System.out.println(ConstantsSummary.MISTAKE_MESSAGE_PLATFORM);

            nameOfPlatform = getUserInfo("Enter a name of platform:");

            deletePlatform(nameOfPlatform);
        }
    }

    public void addAdvertisementToSelectedPlatform(String nameOfBlurbFile) {

        System.out.println("Choosing platform for adding blurb:");

        Path pathToBlurbFile = Paths.get(ConstantsSummary.CURRENT_DIR + nameOfBlurbFile);

        try {
            Os os = Os.valueOf(getUserInfo("Enter an OS").toUpperCase());

            Browser browser = Browser.valueOf(getUserInfo("Enter a browser").toUpperCase());

            Platform platform = platforms.stream()
                    .filter(p -> p.getOs() == os && p.getBrowser() == browser).findFirst().get();

            platform.getDisplays().stream().filter(this::testIfFileEmpty).forEach(d -> {

                changeFiles(pathToBlurbFile, d);

                platform.increaseCountOfBlurbDisplays();
            });

        } catch (IllegalArgumentException e) {

            System.out.println(ConstantsSummary.MISTAKE_MESSAGE_PLATFORM);

            addAdvertisementToSelectedPlatform(nameOfBlurbFile);
        }
    }

    void addNewDisplayToPlatform(String nameOfPlatform, int countOfdDisplayToAdd) {

        Platform selectedPlatform = selectPlatform(nameOfPlatform);

        selectedPlatform.createNewDisplay(countOfdDisplayToAdd);

        selectedPlatform.setCountOfDisplays(selectedPlatform.getCountOfDisplays() + countOfdDisplayToAdd);
    }

    private Predicate<Platform> compareNames(String name) {
        return p -> p.getName().equalsIgnoreCase(name);
    }

    private Platform selectPlatform(String nameOfPlatform) {

        Platform selectedPlatform = null;

        try {

            selectedPlatform = platforms.stream().filter(compareNames(nameOfPlatform)).findFirst().get();

        } catch (NoSuchElementException e) {

            System.out.println(ConstantsSummary.MISTAKE_MESSAGE_PLATFORM);
            nameOfPlatform = getUserInfo("Enter a name of platform:");
            selectPlatform(nameOfPlatform);
        }
        return selectedPlatform;
    }

    private String getUserInfo(String message) {
        System.out.println(message);
        return SCANNER.nextLine();
    }

    private boolean testIfFileEmpty(Path pathToTestingFile) {
        return pathToTestingFile.toFile().length() == 0;
    }

    void changeBlurb(String oldBlurbFileName, String newBlurbFileName) {
        try {
            Path newBlurbPath = createPath(newBlurbFileName);
            Path oldBlurbPath = createPath(oldBlurbFileName);

            String nameOfPlatform = getUserInfo("Enter a name of platform");

            selectPlatform(nameOfPlatform).getDisplays()
                    .stream()
                    .filter(d -> compareFiles(d, oldBlurbPath))
                    .forEach(d -> changeFiles(newBlurbPath, d));

        } catch (FileExistException e) {

            System.out.println(ConstantsSummary.MISTAKE_MESSAGE_FILE);

            oldBlurbFileName = getUserInfo("Enter name of old reclame file");
            newBlurbFileName = getUserInfo("Enter name of new reclame file");

            changeBlurb(oldBlurbFileName, newBlurbFileName);
        }
    }

    private boolean compareFiles(Path pathTofirstFile, Path pathToSecondFile) {

        boolean flag = false;

        try {

            flag = FileUtils.contentEquals(pathTofirstFile.toFile(), pathToSecondFile.toFile());

        } catch (IOException e) {

            e.printStackTrace();
        }
        return flag;
    }

    private void changeFiles(Path pathToSourseFile, Path pathToTargetFile) {
        try {

            Files.copy(pathToSourseFile, pathToTargetFile, StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    void changeConfigOfPlatform(String oldNameOfPlatform, Os newOs, Browser newBrowser, int newCountOfDisplays) {

        Platform selectedPlatform = selectPlatform(oldNameOfPlatform);

        Consumer<Path> delete = p -> {
            try {

                Files.deleteIfExists(p);

            } catch (IOException e) {

                e.printStackTrace();
            }
        };

        selectedPlatform.getDisplays().stream().forEach(delete);
        selectedPlatform.getDisplays().clear();
        selectedPlatform.clearCountersOfDisplays(newCountOfDisplays);
        selectedPlatform.setOs(newOs);
        selectedPlatform.setBrowser(newBrowser);
        selectedPlatform.createNewDisplay(newCountOfDisplays);
    }

    void showPlatformInfo(String nameOfPlatform) {

        Platform selectedPlatform = selectPlatform(nameOfPlatform);
        System.out.println(selectedPlatform);
    }

    private void serializePlatform(String nameOfPlatform) throws IOException {
        Platform selectedPlatform = selectPlatform(nameOfPlatform);

        FileOutputStream outputStream = new FileOutputStream(ConstantsSummary.START_DIR +
                nameOfPlatform +
                ConstantsSummary.SERIALAZED_FILE);

        try (FileOutputStream fos = outputStream;
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(selectedPlatform);
        }

    }

    private void deserializePlatform(String nameOfPlatform) throws IOException, ClassNotFoundException {

        try (FileInputStream fis = new FileInputStream(ConstantsSummary.START_DIR +
                nameOfPlatform + ConstantsSummary.SERIALAZED_FILE);

             ObjectInputStream ois = new ObjectInputStream(fis)) {
            Platform deserializedPlatform = (Platform) ois.readObject();

            System.out.println(deserializedPlatform.getName() + " "
                    + deserializedPlatform.getOs() + " "
                    + deserializedPlatform.getBrowser() + ", count of clear displays = "
                    + deserializedPlatform.getCountOfClearDisplays());
        }
    }

    Path createPath(String nameOfFile) throws FileExistException {

        Path pathToFile = Paths.get(nameOfFile);

        if (!Files.exists(pathToFile)) {

            throw new FileExistException();
        }
        return pathToFile;
    }
}
