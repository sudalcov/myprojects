package homeworks.internet_shop;

public enum Categories {
    TELEPHONE("Telephone"),
    TV_SET("TV set"),
    COMPUTER("Computer"),
    NOTEBOOK("Notebook"),
    AUDIO("Audio centre"),
    WASHING("Washing machine"),
    REFRIGERATOR("Refrigerator"),
    UNSPECIFIED("Unspecified");
    
    private String category;

    Categories(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "category= " + category;
    }
}
