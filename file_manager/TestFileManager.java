package homeworks.file_manager;

import com.itextpdf.text.DocumentException;

import java.io.IOException;
import java.util.Scanner;

/**
 * The TestFileManager program implements an application that provides an ability to work with file system.
 * It makes such actions with files and directories:
 * - creates a new folder
 * - creates a new file
 * - renames file or directory
 * - move file to another directory
 * - copies file to another directory
 * - deletes file or directory
 * - shows a list of files in specified directory
 * - converts file into PDF format
 *
 *
 * @author Udaltsov Sergey
 * @version 1.0
 * @since october 2017.
 * */

public class TestFileManager {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        FileService fileService = new FileService();

        while (true) {
            menu();

            System.out.println("Enter a number of action: ");

            int key = SCANNER.nextInt();

            switch (key) {

                case 1: {
                    fileService.createNewDirectory();
                    break;
                }
                case 2: {
                    String nameOfFile = getUserInfo("Enter a path to file: ");

                    fileService.createNewFile(nameOfFile);
                    break;
                }
                case 3: {
                    String nameOfOldFile = getUserInfo("Enter a path to old file: ");

                    String nameOfNewFile = getUserInfo("Enter a path to new file: ");

                    fileService.renameOrMove(nameOfOldFile, nameOfNewFile);
                    break;
                }
                case 4: {
                    String nameOfOldFile = getUserInfo("Enter a path to old file: ");

                    String nameOfNewFile = getUserInfo("Enter a path to new file: ");

                    fileService.renameOrMove(nameOfOldFile, nameOfNewFile);

                    break;
                }
                case 5: {
                    String nameOfFile = getUserInfo("Enter a path to old position of file: ");

                    String nameOfNewFilePosition = getUserInfo("Enter a path to new position of file: ");

                    fileService.copy(nameOfFile, nameOfNewFilePosition);

                    break;
                }
                case 6: {
                    String nameOfFile = getUserInfo("Enter a name of file or directory");

                    fileService.delete(nameOfFile);
                    break;
                }
                case 7: {
                    String nameOfDirectory = getUserInfo("Enter a name of directory");
                    fileService.showListOfFiles(nameOfDirectory);
                    break;
                }
                case 8: {
                    String nameOfFile = getUserInfo("Enter a path to file");
                    try {
                        fileService.convertToPdf(nameOfFile);
                    } catch (DocumentException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case 9: {
                    return;
                }
                default: {
                    System.err.println("Sorry, you've entered a wrong number. Try again.");
                    break;
                }

            }
        }

    }

    private static void menu() {
        System.out.println("1) Create new folder \n" + "" +
                "2) Create new file \n" + "" +
                "3) Rename file or folder \n" + "" +
                "4) Move file \n" + "" +
                "5) Copy file \n" + "" +
                "6) Delete file or folder \n" + "" +
                "7) Directory list\n" + "" +
                "8) Convert file to PDF format\n" + "" +
                "9) Exit");
    }

    private static String getUserInfo(String message) {

        System.out.println(message);

        return SCANNER.nextLine();
    }
}
