SHOW DATABASES;

USE homeworks;


SHOW TABLE;

USE lessons;

CREATE TABLE product(
product_id int AUTO_INCREMENT,
product_name varchar(50),
manufacture_date date,
producer int,
strength enum('fragile', 'strong'),
price double,
PRIMARY KEY (product_id),
FOREIGN KEY (producer) REFERENCES producer(producer_id)

);

CREATE TABLE producer(
producer_id int AUTO_INCREMENT,
brand varchar(50),
country varchar(25),
PRIMARY KEY (producer_id)
);

DROP TABLE producer;

DROP TABLE product;

INSERT INTO producer(brand, country) VALUES
("Sony", "Japan"),
("Samsung", "Korea"),
("BMW", "Germany"),
("Lamborgini", "Italy");

INSERT INTO product(product_name, manufacture_date, producer, strength, price) VALUES
("Telephone", "2012.8.15", 2, "fragile", 2500),
("TV set", "2017.5.25", 1, "fragile", 10000),
("telephone", "2015.3.12", 1, "fragile", 1500),
("notebook", "2013.9.5", 2, "fragile", 7000),
("x5", "2013.6.13", 3, "strong", 150000),
("m3", "2015.7.21", 3, "strong", 120000),
("galardo", "2017.6.26", 4, "strong", 350000),
("diablo", "2016.1.10", 4, "strong", 400000);

DROP TABLE product;
DELETE FROM product;



SELECT * FROM producer;

SELECT * FROM product WHERE producer=1 AND strength = "fragile";
SELECT * FROM product WHERE manufacture_date BETWEEN "2013.01.01" AND "2017.01.01";
SELECT * FROM product WHERE price BETWEEN 10000 AND 200000;


SELECT * FROM product WHERE product_name LIKE 't%';
SELECT * FROM product WHERE product_name LIKE 'tel%';

SELECT * FROM product ORDER BY manufacture_date ASC LIMIT 5;
SELECT * FROM product ORDER BY manufacture_date DESC LIMIT 5;



SELECT * FROM product;

SELECT concat(m.product_name, '123') FROM product AS m;

CREATE TABLE man(
man_id int AUTO_INCREMENT,
first_name varchar(50) NOT NULL,
last_name varchar(50),
count_of_children TINYINT DEFAULT 0,
address_id INT,
PRIMARY KEY(man_id),
FOREIGN KEY (address_id) REFERENCES address(address_id)
);


CREATE TABLE address(
address_id int AUTO_INCREMENT,
street varchar(50) NOT NULL UNIQUE,
city varchar(50) NOT NULL,
PRIMARY KEY(address_id)
);


INSERT INTO address(street, city) VALUES 
('NUllAddress1', 'Dnepr2'),
('NUllAddress2', 'Dnepr2');

SELECT * FROM man;

SELECT * FROM address;

SELECT * FROM man WHERE count_of_children > 1 AND first_name = "John4";

SELECT * FROM man WHERE count_of_children < 4 AND count_of_children > 0 OR first_name = "John4";

SELECT DISTINCT man_id, first_name FROM man;

SELECT * FROM man ORDER BY count_of_children ASC;

SELECT * FROM man ORDER BY count_of_children DESC;

DROP TABLE man;

INSERT INTO man(first_name, last_name, count_of_children, address_id)
VALUES('John', 'Lenon1', 2, 2);

UPDATE man SET first_name = 'Av', last_name = 'sudalcov' WHERE man_id = 2;

SELECT * FROM man;

SELECT * FROM man ORDER BY count_of_children DESC LIMIT 1;

SELECT *, max(count_of_children) FROM man;

SELECT count(*) AS count_of_rows, sum(count_of_children) FROM man;

SELECT first_name FIO FROM man;

SELECT 12;

DELETE FROM man WHERE man_id = 2;

SELECT * FROM man WHERE first_name LIKE 'J%';

SELECT * FROM man WHERE first_name LIKE '%2';

SELECT * FROM man WHERE first_name LIKE '%o%';


SELECT * FROM man WHERE man_id = 1 OR man_id = 2 OR man_id = 3;

SELECT * FROM man WHERE man_id IN(1, 2, 3);

SELECT * FROM man WHERE count_of_children BETWEEN 2 AND 5;

SELECT * FROM man WHERE count_of_children IN (SELECT count_of_children FROM man WHERE count_of_children BETWEEN 2 AND 5);


# Joins

/*Inner join*/

SELECT m.first_name, a.city FROM man m
INNER JOIN address a 
ON m.address_id = a.address_id;

/*Left join*/

SELECT *
FROM address a
LEFT JOIN man m 
ON m.address_id = a.address_id
WHERE m.man_id IS NULL;

/*Right join*/

SELECT *
FROM man m 
RIGHT JOIN address a 
ON m.address_id = a.address_id;

/*Cross join*/

SELECT * FROM man CROSS JOIN address;

SELECT * FROM man;

SELECT * FROM address;

/*Alters*/

/*Add column*/

ALTER TABLE address
ADD count_citizens INT DEFAULT 10;

ALTER TABLE man
ADD status1 varchar(30) DEFAULT 'Father';

/*Drop column*/

ALTER TABLE man
DROP COLUMN status;

/*Change datatype of column*/

ALTER TABLE man
MODIFY COLUMN status1 varchar(100);

/*Group by*/
SELECT *, sum(count_citizens) FROM address
GROUP BY street, city
HAVING sum(count_citizens) > 10;

/*Functions*/

SELECT max(count_citizens) FROM address;
SELECT min(count_citizens) FROM address;

/*Dates*/

SELECT now();
SELECT curdate();
SELECT curtime();


SELECT date_add(curdate(), INTERVAL 2 YEAR);

SELECT date_sub(curdate(), INTERVAL 2 YEAR);


SELECT date_format(now(), '%b %d %Y %h:%i %p');

SELECT date_format(now(), '%m-%d-%Y');

/*Cases*/

SELECT first_name, last_name, (CASE 
WHEN count_of_children = 1 THEN 'man has 1 children'
WHEN count_of_children = 2 THEN 'man has 2 children'
END) AS count_of_children FROM man;