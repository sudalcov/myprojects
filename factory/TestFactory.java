package homeworks.from_andrew.factory;


import homeworks.from_andrew.factory.stages.Frame;
import homeworks.from_andrew.factory.stages.Leg;
import homeworks.from_andrew.factory.stages.Tire;
import homeworks.from_andrew.factory.stages.Wheel;

/**
 * The TestFactory program implements an application that imitates a work of some factory, which produces a different
 * kinds of goods.
 * The application provides possibility to make an consumer's order in which the worker and good can be specified.
 * Also every stage of executing can be shown in a log file.
 * Consumer may refuse going on producing a good. He must switch the reason of refusing.
 * When the good is produced the final record is made to the log file.
 * Every good is being made by the different worker(thread)
 *
 * @author Udaltsov Sergey
 * @version 1.0
 * @since december 2017.
 * */

public class TestFactory {
    public static void main(String[] args) throws InterruptedException {

        Stageable frame = new Frame();

        Stageable wheel = new Wheel();

        Stageable tire = new Tire();

        Good bicycle = new Good("Bike");

        Good sofa = new Good("Sofa");

        sofa.addAction(new Leg());

        sofa.addAction(new Frame());

        bicycle.addAction(frame);

        bicycle.addAction(wheel);

        bicycle.addAction(tire);

        Worker worker = new Worker(bicycle, "Zlodej");

        Worker sofaWorker = new Worker(sofa, "John");

        Thread.sleep(1500);

        sofaWorker.printCurrentStage();

        worker.setReasonForStop("Bad quality");

        worker.stopWork();

    }
}
