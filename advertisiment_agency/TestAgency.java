package homeworks.advertisiment_agency;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * The TestAgency program implements an application that imitates a work of advertisement agency.
 * The agency has 5 platforms with several displays for advertisement. Each platform works with it's own browser and OS.
 * Each platform is a directory with 5 displays(files) for advertisement and 1 file for serialised information about
 * this platform(browser, OS, name of platform, number of files with advertisement and clear files).
 * An application provides ability to:
 * - set advertisement of customer only on platform with specified browser and OS
 * - change certain advertisement on the certain platform
 * - create a new platform
 * - create and set a new advertisement(text)
 * - delete a platform with all the advertisements
 * - change configuration of platform with deleting all the advertisements
 * - add a display to certain platform
 *
 *
 * @author Udaltsov Sergey
 * @version 1.0
 * @since december 2017.
 * */

public class TestAgency {
    public static void main(String[] args) throws IOException, ClassNotFoundException {


        AgencyService service = new AgencyService();

        service.deletePlatform("");

        try {
            Files.createDirectory(Paths.get(ConstantsSummary.START_DIR));
        } catch (IOException e) {
            e.printStackTrace();
        }

        service.createNewPlatform("Platform1", Browser.FIREFOX, Os.WINDOWS, 5);
        service.createNewPlatform("Platform2", Browser.INTERNET_EXPLORER, Os.WINDOWS, 5);
        service.createNewPlatform("Platform3", Browser.MOZILLA, Os.MAC_OS, 5);
        service.createNewPlatform("Platform4", Browser.OPERA, Os.LINUX, 5);
        service.createNewPlatform("Platform5", Browser.SAFARY, Os.IOS, 5);

        String text = "uhuyuukmodjeojfoijf";

        Path path1 = Paths.get("./Advertisment/Platform1/File1.txt");
        Path path2 = Paths.get("./Advertisment/Platform1/File2.txt");
        Path path3 = Paths.get("./Advertisment/Platform2/File2.txt");
        Path path4 = Paths.get("./Advertisment/Platform3/File2.txt");
        Path path5 = Paths.get("./Advertisment/Platform4/File2.txt");
        Path path6 = Paths.get("./Advertisment/Platform5/File2.txt");

        try {
            Files.write(path1, text.getBytes(), StandardOpenOption.APPEND);
            Files.write(path2, text.getBytes(), StandardOpenOption.APPEND);
            Files.write(path3, text.getBytes(), StandardOpenOption.APPEND);
            Files.write(path4, text.getBytes(), StandardOpenOption.APPEND);
            Files.write(path5, text.getBytes(), StandardOpenOption.APPEND);
            Files.write(path6, text.getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            e.printStackTrace();
        }
        service.addAdvertisementToSelectedPlatform("Blurb.txt");
        service.addAdvertisementToSelectedPlatform("Blurb.txt");
       service.deletePlatform("Platfor1");
       service.changeBlurb("./Blurb15.txt", "./Blurb1.txt");
       service.deletePlatform("Platform1");
       service.changeConfigOfPlatform("Platform1", Os.IOS, Browser.OPERA, 4);
        service.addNewDisplayToPlatform("Platform1",2);
        Files.write(path1, text.getBytes(), StandardOpenOption.APPEND);
        Files.write(path2, text.getBytes(), StandardOpenOption.APPEND);
        service.addAdvertisementToSelectedPlatform("Blurb1.txt");
        service.showPlatformInfo("Platform1");
    }
}
